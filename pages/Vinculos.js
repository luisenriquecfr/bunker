import React, { Component } from 'react';
import { Container , Content, List, ListItem, Left, Body, Right, Thumbnail, CardItem, Button, Card, Input, Item, Icon  } from 'native-base';
import { Avatar, Tile, Header, Divider  } from 'react-native-elements'
import { AsyncStorage , Text, Dimensions, View, StyleSheet, TouchableOpacity, Image, AppRegistry, ScrollView, Platform, UIManager, LayoutAnimation, RefreshControl, Alert } from 'react-native'
import { Col, Row, Grid } from 'react-native-easy-grid';
import { StackNavigator } from 'react-navigation';
import ActionButton from 'react-native-action-button';
import Modal from 'react-native-modal';
import { ImagePicker, Components, BlurView } from 'expo';
import Animation from 'lottie-react-native';
import TimerMixin from 'react-timer-mixin';
import Display from 'react-native-display';
import Communications from 'react-native-communications';
let window = Dimensions.get('window')
UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);
export default class Vinculos extends Component {
constructor(props){
    super(props);
    this.navigation = this.props.navigation;
    this.params = this.props.navigation.state;
    this.timeouts = [];
    this.sectores = [];
    this.red = [];
    this.vinculos = [];
    this.state = {
        navigation: this.props.navigation,
        timePassed: false,
        idUsuario: this.navigation.state.params.idUsuario,
        idRed: this.navigation.state.params.idRed,
        nombreRed: this.navigation.state.params.nombreRed,
        isActionButtonVisible: true,
        isModalVisible: false,
        isModalVisibleVinculo: false,
        vinculoNombre: null,
        vinculoApellido: null,
        vinculoTelefono: null,
        vinculoMail: null,
        vinculoEmpresa: null,
        vinculoPuesto: null,
        refreshing: false
  }
  console.log('///////// Inside Vinculos Props' + this.state.idUsuario);
  console.log('///////// Inside Vinculos Props' + this.state.idRed);
  console.log('///////// Inside Vinculos Props' + this.state.nombreRed);
}

static navigationOptions  = ({navigation}) => {
    const { params = {} } = navigation.state
    const { nav = {} } = navigation;
    return{
    title: 'Vinculos',
    headerTintColor: '#FFF',
headerRight:  (Platform.OS === 'ios' ) ? <TouchableOpacity onPress={()=>{params.handleSave()}}
 ><Icon type='ionicon' name='ios-add'  size={35} style={{ marginRight: 20, color: '#fff' }} /></TouchableOpacity> : null,
     headerStyle: {
        backgroundColor: '#41413f'
    }, 
}
}

async goToNav(){
    const DEMO_TOKEN = await AsyncStorage.getItem('id_token');
    this.navigation.navigate('crearVinculo', { idUsuario: this.state.idUsuario, idRed: this.state.idRed, onGoBack: ()=> this.reloadView() })
}

reloadView(){
    this.fetchProspectos();
    this.forceUpdate(); 
}

  _onRefresh() {
    this.setState({refreshing: true});
    fetch('http://innovatio.mx/reactAcem/getVinculos.php',{
            method: 'POST',
            header: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                idUsuario: this.state.idUsuario,
                idRed: this.state.idRed
            })
        }).then(( response )=> response.json())
        .then(( responseJSON )=> { 
              if(responseJSON.result == 'vacio'){

              }
              else{
                  this.vinculos = responseJSON
                  console.log('/// Refresh (:');
              }
         })
        .catch((error) => {
        console.error(error);
      });
this.animation.play();
 this.timeouts.push(setTimeout(() => {
    this.setState({timePassed: true});
    console.log(this.timePassed);
  }, 3250));
    this.setState({refreshing: false});
  }

fetchProspectos(){
     fetch('http://innovatio.mx/reactAcem/getVinculos.php',{
            method: 'POST',
            header: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                idUsuario: this.state.idUsuario,
                idRed: this.state.idRed
            })
        }).then(( response )=> response.json())
        .then(( responseJSON )=> { 
              if(responseJSON.result == 'vacio'){
 
              }
              else{
                  this.vinculos = responseJSON
                  console.log('/// Inside Vinculo Data' + this.vinculos);
              }
         })
        .catch((error) => {
        console.error(error);
      });
}

componentDidMount() {
this.props.navigation.setParams({ handleSave: this.goToNav.bind(this) });
    fetch('http://innovatio.mx/reactAcem/getVinculos.php',{
            method: 'POST',
            header: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                idUsuario: this.state.idUsuario,
                idRed: this.state.idRed
            })
        }).then(( response )=> response.json())
        .then(( responseJSON )=> { 
              if(responseJSON.result == 'vacio'){

              }
              else{
                  this.vinculos = responseJSON
                  console.log('/// Inside Vinculo Data' + this.vinculos);
              }
         })
        .catch((error) => {
        console.error(error);
      });
this.animation.play();
 this.timeouts.push(setTimeout(() => {
    this.setState({timePassed: true});
    console.log(this.timePassed);
  }, 3250));
}

deleteVinculo(idVinculo){
    fetch('http://innovatio.mx/reactAcem/deleteVinculo.php',{
            method: 'POST',
            header: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                idUsuario: this.state.idUsuario,
                idVinculo: idVinculo
            })
        }).then(( response )=> response.json())
        .then(( responseJSON )=> { 
              if(responseJSON.result == 'vacio'){

              }
              else{
                Alert.alert(
                  'Éxito',
                  'Se ha borrado exitosamente el Vinculo',
                  [
                    {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                    {text: 'OK', onPress: () => console.log('OK Pressed')},
                  ],
                  { cancelable: false }
                )
              }
         })
        .catch((error) => {
        console.error(error);
      });   
}

clearTimeouts(){
  this.timeouts.forEach(clearTimeout);
}
  
componentWillUnmount(){
  this.clearTimeouts();
}

  _showModal = () => this.setState({ isModalVisible: true })

  _hideModal = () => this.setState({ isModalVisible: false })

  _showModalV = () => this.setState({ isModalVisibleVinculo: true })

  _hideModalV = () => this.setState({ isModalVisibleVinculo: false })

_listViewOffset = 0


_onScroll = (event) => {
  // Simple fade-in / fade-out animation
  const CustomLayoutLinear = {
    duration: 300,
    create: { type: LayoutAnimation.Types.easeInEaseOut, property: LayoutAnimation.Properties.opacity },
    update: { type: LayoutAnimation.Types.easeInEaseOut, property: LayoutAnimation.Properties.opacity },
    delete: { type: LayoutAnimation.Types.easeInEaseOut, property: LayoutAnimation.Properties.opacity }
  }
  // Check if the user is scrolling up or down by confronting the new scroll position with your own one
  const currentOffset = event.nativeEvent.contentOffset.y
  const direction = (currentOffset > 0 && currentOffset > this._listViewOffset)
    ? 'down'
    : 'up'
  // If the user is scrolling down (and the action-button is still visible) hide it
  const isActionButtonVisible = direction === 'up'
  if (isActionButtonVisible !== this.state.isActionButtonVisible) {
    LayoutAnimation.configureNext(CustomLayoutLinear)
    this.setState({ isActionButtonVisible })
  }
  // Update your scroll position
  this._listViewOffset = currentOffset
}
getFab(){
    return (Platform.OS === 'android') ? <ActionButton onPress={()=> this.goToNav()} buttonTextStyle={{ color: '#41413f' }} buttonColor="#fff"></ActionButton> : null 
}


  render() {
    Swipeout = require('react-native-swipeout').default;
    
    return (
      <Container>
        <Content onScroll={this._onScroll}
        refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this._onRefresh.bind(this)}
          />
        }>
       <List>
            {
                this.vinculos.map((l, i) => (
              <Swipeout
                key={"swipe" + i}
                style={{
                backgroundColor: 'white'
              }}
                right={[
                {
                  text: <Icon
                    style={{
                    fontSize: 27,
                    color: 'white'
                  }}
                    ios='ios-create-outline'
                    android='md-create'
                    color='white'></Icon>,
                  backgroundColor: '#41413f',
                  onPress: ()=> this.navigation.navigate('crearVinculo', { idUsuario: this.state.idUsuario, idRed: this.state.idRed, update: 'true', idVinculo: l.id })
                },
                {
                  text: <Icon
                    style={{
                    fontSize: 27,
                    color: 'white'
                  }}
                    ios='ios-mail-outline'
                    android='md-mail'
                    color='white'></Icon>,
                  backgroundColor: 'rgb(76,217,100)',
                  onPress: ()=> Communications.email([l.correo_vinculo], null, null, null, null)
                }, {
                  text: <Icon
                    style={{
                    fontSize: 27,
                    color: 'white'
                  }}
                    ios='ios-call-outline'
                    android='md-call'
                    color='white'></Icon>,
                  backgroundColor: 'rgb(0,122,255)',
                  onPress: ()=> 
                  Alert.alert(
                    "Llamar a:",
                    null, 
                    [
                      {text: "Celular", onPress: ()=>Communications.phonecall(l.telefono_vinculo, true)},
                      {text: "Cancelar"}
                    ]
                  )
                  

                }, {
                  text: <Icon
                    style={{
                    fontSize: 27,
                    color: 'white'
                  }}
                    ios='ios-trash-outline'
                    android='md-trash'
                    color='white'></Icon>,
                  backgroundColor: 'rgb(255,59,48)',
                  onPress: ()=> Alert.alert("¿Estas seguro de que quieres borrar este Vinculo?", "Se borrarán todos los datos asociados a este prospecto",
                  [
                    {text: "Cancelar"},
                    {text: "Aceptar", onPress: ()=> this.deleteVinculo(l.id)}
                  ]
                  )
                }
              ]}>
                    <ListItem avatar>
                    <Left>
                        <Avatar
                        medium
                        rounded
                        key={i}
                        title={(l.nombre_vinculo).charAt(0)}
                        onPress={() => console.log("Works!")}
                        activeOpacity={0.7}
                        overlayContainerStyle={{backgroundColor: '#cfcabb'}}
                        />
                    </Left>
                    <Body>
                        <Text><Icon style={{fontSize: 24, color: '#41413f'}} ios='ios-contact-outline' android='md-contact' color='white'></Icon>  {l.nombre_vinculo} {l.apellido_pat_vinculo} {l.apellido_mat_vinculo}</Text>
                        <Text><Icon style={{fontSize: 24, color: '#41413f'}} ios='ios-briefcase-outline' android='md-briefcase' color='white'></Icon>  {l.puesto_vinculo}</Text>
                        <Text note><Icon style={{fontSize: 24, color: '#41413f'}} ios='ios-link-outline' android='md-link' color='white'></Icon>  {l.nombre_enlace}</Text>
                    </Body>
                    </ListItem>
                  </Swipeout>
                ))
            }
            </List>
        </Content>
            {this.state.isActionButtonVisible ? this.getFab() : null} 
             <Display keepAlive={true} enter={'fadeIn'} exit={'fadeOut'}  enable={!this.state.timePassed} style={{position: 'absolute', zIndex: 100, width: window.width, height: window.height}}>
                <BlurView tint="dark" intensity={100} style={{position: 'absolute', zIndex: 100, width: window.width, height: window.height}} >
                    <View style={{ flex: 1, justifyContent: 'center', alignContent: 'center' }}>
                        <View
                            style={{
                            justifyContent: 'center', alignContent: 'center',width: window.width , marginLeft: window.width/2-100
                            }}>
                        <Animation
                        style={{
                            width:200,
                            height:200 ,
                        }}
                        source={require('../assets/data12.json')}
                        loop
                        ref={(c)=> this.animation = c}
                        />
                        </View>
                        <Text style={{ fontSize: 24, textAlign: 'center', color:'white', width: window.width }}>Cargando...</Text>
                    </View>  
                </BlurView>
          </Display>
        </Container>
    );
  }
}


var styles = StyleSheet.create({
    headerText: {
        fontSize: 22,
        fontWeight: '100',
        color: '#fff',
        textAlign: 'center',
        marginTop: 8
    },
    headerJob: {
        fontSize: 16,
        fontWeight: '100',
        color: '#fff',
        textAlign: 'center'
    },
    headerTile: {
        fontSize: 16,
        fontWeight: '100',
        color: 'rgba(0,0,0,.6)',
        marginTop: 15,
        textAlign: 'center'
    },
    columnas: {
        borderWidth: 1, 
        borderColor: 'rgba(0,0,0,.1)',
        backgroundColor: '#fff',  
        justifyContent: 'center', 
        alignItems: 'center',
        height: 120
    },
    profile: {
        justifyContent: 'center', 
        alignItems: 'center', 
        marginTop: 22
    },
    actionButtonIcon: {
        height: 22,
        color: 'white',
    },
    container: {
        flex: 1,
        padding: 10
    },
    backgroundImage: {
        width: null,
        height: 300,
    },
    overlay: {
        position: 'absolute',
        top: 0,
        right: 0,
        bottom: 0,
        left: 0,
        backgroundColor: 'red',
        opacity: 0.3
    },
    modal: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    modal4: {
        height: 300
    },
    text: {
        color: "black",
        fontSize: 22
    },
    btn: {
        margin: 10,
        backgroundColor: "#3B5998",
        color: "white",
        padding: 10
    },

    btnModal: {
        position: "absolute",
        top: 0,
        right: 0,
        width: 50,
        height: 50,
        backgroundColor: "transparent"
    },
    button: {
        backgroundColor: 'lightblue',
        padding: 12,
        margin: 16,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        borderColor: 'rgba(0, 0, 0, 0.1)',
    },
    modalHeaderProspecto: {
        backgroundColor: '#3498db',
        padding: 10,
        borderColor: 'rgba(0, 0, 0, 0.1)',
        height: 200
    },
    modalHeaderVinculo: {
        backgroundColor: 'rgba(189,9,38,1)',
        padding: 10,
        borderColor: 'rgba(0, 0, 0, 0.1)',
        height: 200
    },
    modalAvatar: {
        alignItems: 'center',
        flex: 1
    },
    modalContent: {
        backgroundColor: '#fff',
        height: 220,
        borderColor: 'rgba(0, 0, 0, 0.1)'
    },
    bottomModal: {
        justifyContent: 'flex-end',
        margin: 0,
    }
});
AppRegistry.registerComponent("Vinculos", ()=> Vinculos);
