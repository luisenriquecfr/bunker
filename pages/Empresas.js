import React, { Component } from 'react';
import { Container , Content, List, ListItem, Left, Body, Right, Thumbnail, CardItem, Button, Input, Form, Item, Label, Icon } from 'native-base';
import { Avatar, Tile, Header, Card, Divider  } from 'react-native-elements'
import { Text, Dimensions, View, StyleSheet, TouchableOpacity, Image, AppRegistry, ScrollView, Platform, UIManager, LayoutAnimation, Alert, AlertIOS } from 'react-native';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { StackNavigator } from 'react-navigation';
import { ImagePicker, Components, BlurView } from 'expo';
import Animation from 'lottie-react-native';
import TimerMixin from 'react-timer-mixin';
import Display from 'react-native-display';
let window = Dimensions.get('window')
import ActionButton from 'react-native-action-button';
import Modal from 'react-native-modal';
UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);
const demo = [{
 nombre_empresa: 'Crea una Empresa'
}];
export default class Empresas extends Component {
constructor(props){
    super(props);
    this.navigation = this.props.navigation;
    this.params = this.props.navigation.state;
    this.timeouts = [];
    this.empresas = [];
    this.state = {
      isActionButtonVisible: true,
      timePassed: false,
      idSector: this.navigation.state.params.idSector,
      nombreSector: this.navigation.state.params.nombre_sector,
      idUsuario: this.navigation.state.params.idUsuario,
      empresaPrompt: null,
      isModalVisible: false,
      idEmpresa: null
  };
}

fetchEmpresas(){
     fetch('http://innovatio.mx/reactAcem/getEmpresaAlianza.php',{
        method: 'POST',
        header: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                idUsuario: this.navigation.state.params.idUsuario,
                idSector: this.navigation.state.params.idSector
            })
        }).then((response)=> response.json())
        .then(( responseJSON )=> { 
              if(responseJSON.result == 'vacio'){

              }
              else{
                  this.empresas = responseJSON
                  console.log('/// Inside Empresa Data' + this.empresas);
              }
         })
        .catch((error) => {
        console.error(error);
      });
}

 componentDidMount() {
this.props.navigation.setParams({ handleSave: this.showPrompt.bind(this) });
    fetch('http://innovatio.mx/reactAcem/getEmpresaAlianza.php',{
        method: 'POST',
        header: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                idUsuario: this.state.idUsuario,
                idSector: this.navigation.state.params.idSector
            })
        }).then((response)=> response.json())
        .then(( responseJSON )=> { 
              if(responseJSON.result == 'vacio'){

              }
              else{
                  this.empresas = responseJSON
                  console.log('/// Inside Empresa Data' + this.empresas);
              }
         })
        .catch((error) => {
        console.error(error);
      });
  this.animation.play();
  this.timeouts.push(setTimeout(() => {
      this.setState({ timePassed: true });
    }, 3250));
}

clearTimeouts(){
  this.timeouts.forEach(clearTimeout);
}
  
componentWillUnmount(){
  this.clearTimeouts();
}

  _showModal = () => this.setState({ isModalVisible: true })

  _hideModal = () => this.setState({ isModalVisible: false })


 showPrompt(){
    AlertIOS.prompt(
  'Crear nueva empresa',
  'Introduce el nombre de la nueva empresa',[
    {text: 'Cancelar', onPress: (text)=> {this.setState({empresaPrompt: null})} },
    {text: 'Aceptar', onPress: (text)=> {console.log(text); this.setState({empresaPrompt: text}); this.createEmpresa(this.state.empresaPrompt) } }
  ]
);
    }

createEmpresa(empresa){
if ( empresa == null || empresa == '' ){
Alert.alert(
  'Error',
  'Revisa que hayas llenado correctamente el nombre de la empresa',
  [
    {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
    {text: 'OK', onPress: () => console.log('OK Pressed')},
  ],
  { cancelable: false }
)
}
else{
this._hideModal();
console.log('Inside Create Sector Nombre Empresa' + empresa);
console.log('//// Create Sector Usuario' + this.state.idUsuario);
console.log('After Sctor Init' + this.state.idSector);
    fetch('http://innovatio.mx/reactAcem/createEmpresa.php',{
        method: 'POST',
        header: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                idSector: this.state.idSector,
                idUsuario: this.state.idUsuario,
                nombre_empresa: empresa
            })
        }).then((response)=> response.json())
        .then(( responseJSON )=> { 
              if(responseJSON.result == 'vacio'){
                Alert.alert(
                'Error',
                'Revisa que tengas conexión a internet o vuelve a intentar en un momento',
                [
                    {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                    {text: 'OK', onPress: () => console.log('OK Pressed')},
                ],
                { cancelable: false }
                )
              }
              else{
                Alert.alert(
                'Éxito',
                'La Empresa se ha agregado exitosamente',
                [
                    {text: 'OK', onPress: () => {this.fetchEmpresas(); this.forceUpdate();}},
                ],
                { cancelable: false }
                )
              }
              this.setState({empresaPrompt: null})
         })
        .catch((error) => {
        console.error(error);
      });
}
}

deleteEmpresa(idEmpresa){
    fetch('http://innovatio.mx/reactAcem/deleteEmpresa.php',{
        method: 'POST',
        header: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                idUsuario: this.navigation.state.params.idUsuario,
                idEmpresa: idEmpresa,
            })
        }).then((response)=> response.json())
        .then(( responseJSON )=> { 
              if(responseJSON.result == 'fallo'){
                Alert.alert(
                  'Error',
                  'Error',
                  [
                    {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                    {text: 'OK', onPress: () => console.log('OK Pressed')},
                  ],
                  { cancelable: false }
                )
              }
              else{
                  Alert.alert("Empresa eliminada exitosamente");
                  this.fetchEmpresas();
                  this.forceUpdate();
              }
         })
        .catch((error) => {
        console.error(error);
      }); 
}

static navigationOptions  = ({navigation}) => {
    const { params = {} } = navigation.state
    const { nav = {} } = navigation;
    return{
    title: 'Empresas',
    headerTintColor: '#FFF',
    headerRight:  (Platform.OS === 'ios' ) ? <TouchableOpacity onPress={()=>{params.handleSave()}}
 ><Icon type='ionicon' name='ios-add'  color='white' size={35} style={{ marginRight: 20, color: 'white'}} /></TouchableOpacity> : null,
     headerStyle: {
        backgroundColor: '#41413f'
    }, 
}
}


_listViewOffset = 0


_onScroll = (event) => {
  // Simple fade-in / fade-out animation
  const CustomLayoutLinear = {
    duration: 300,
    create: { type: LayoutAnimation.Types.easeInEaseOut, property: LayoutAnimation.Properties.opacity },
    update: { type: LayoutAnimation.Types.easeInEaseOut, property: LayoutAnimation.Properties.opacity },
    delete: { type: LayoutAnimation.Types.easeInEaseOut, property: LayoutAnimation.Properties.opacity }
  }
  // Check if the user is scrolling up or down by confronting the new scroll position with your own one
  const currentOffset = event.nativeEvent.contentOffset.y
  const direction = (currentOffset > 0 && currentOffset > this._listViewOffset)
    ? 'down'
    : 'up'
  // If the user is scrolling down (and the action-button is still visible) hide it
  const isActionButtonVisible = direction === 'up'
  if (isActionButtonVisible !== this.state.isActionButtonVisible) {
    LayoutAnimation.configureNext(CustomLayoutLinear)
    this.setState({ isActionButtonVisible })
  }
  // Update your scroll position
  this._listViewOffset = currentOffset
}
getFab(){
    return (Platform.OS === 'android') ? <ActionButton onPress={() => this._showModal() } buttonTextStyle={{ color:'#41413f' }} buttonColor="#fff"></ActionButton> : null 
}
  render() {
         Swipeout = require('react-native-swipeout').default;
            swipeoutBtns = [
                {
                    text: <Icon style={{fontSize: 27, color: 'white'}} ios='ios-trash-outline' android='md-trash' color='white'></Icon>,
                    backgroundColor: 'rgb(255,59,48)'
                }
        ];
    return (
    <Container>
        <Content onScroll={this._onScroll} >
            {
                this.empresas.map((l, i) => (
                  <Swipeout  style={{backgroundColor:'white'}} right={[
                        {
                            text: <Icon style={{fontSize: 27, color: 'white'}} ios='ios-create-outline' android='md-create' color='white'></Icon>,
                            backgroundColor: '#41413f',
                            onPress: ()=> this.navigation.navigate('editEmpresa', {idUsuario: this.state.idUsuario, idEmpresa: l.id, idSector: this.state.idSector})
                        },
                        {
                            text: <Icon style={{fontSize: 27, color: 'white'}} ios='ios-trash-outline' android='md-trash' color='white'></Icon>,
                            backgroundColor: 'rgb(255,59,48)',
                            onPress: ()=> Alert.alert("¿Estas seguro de que quieres borrar esta empresa?", null,
                            [
                                {text: "Cancelar"},
                                {text: "Aceptar", onPress: ()=> this.deleteEmpresa(l.id)}
                            ]
                            )
                        }
                    ]}>
                    <ListItem avatar>
                    <Left>
                        <Avatar
                        medium
                        rounded
                        key={i}
                        title={(l.nombre_empresa).charAt(0)}
                        onPress={() => console.log("Works!")}
                        activeOpacity={0.7}
                        overlayContainerStyle={{backgroundColor: 'rgba(189,9,38,1)'}}
                        />
                    </Left>
                    <Body>
                        <Text><Icon style={{fontSize: 24, color: '#41413f'}} ios='ios-briefcase-outline' android='md-briefcase' color='white'></Icon>  {l.nombre_empresa}</Text>
                        <Text><Icon style={{fontSize: 24, color: '#41413f'}} ios='ios-contact-outline' android='md-contact' color='white'></Icon>  {l.nombreTitular}</Text>
                        <Text><Icon style={{fontSize: 24, color: '#41413f'}} ios='ios-cash-outline' android='md-cash' color='white'></Icon>  ${l.acuerdo}</Text>
                    </Body>
                    </ListItem>
                  </Swipeout>
                ))
            }
        <Modal isVisible={this.state.isModalVisible}>
            <View style={styles.modalHeaderProspecto}>
                <Grid>
                    <Row size={1}>
                    <Col style={{ backgroundColor: 'rgba(255,255,255,.0)', height: 30 }} >
                            <TouchableOpacity
                             onPress={()=> {this._hideModal(); 
                                    this.setState({empresaPrompt: null})}}>
                            <Icon
                                name='close'
                                type='simple-line-icon'
                                color='rgba(0,0,0,.6)'
                                size={30}
                                style={{ justifyContent: 'flex-end' }}
                            />
                        </TouchableOpacity>
                    </Col>
                    <Col style={{ backgroundColor: 'rgba(255,255,255,.0)', height: 30, alignItems: 'flex-end' }} >
                    </Col>
                    </Row>
                    <Row size={4}>
                    <Col style={{ alignItems: 'center', alignContent: 'center', justifyContent: 'center' }} >
                            <Icon
                            name='briefcase'
                            type='simple-line-icon'
                            color='rgba(0,0,0,.6)'
                            size={80}
                            style={{ alignItems: 'center', justifyContent: 'center' }}
                      />
                                <Text style={{ fontSize: 18, fontWeight: '100', color: 'rgba(0,0,0,.6)', textAlign: 'center', justifyContent: 'center', alignItems: 'center', marginTop: 30 }} >
                                Añade el nombre de la Empresa que quieres agregar a este Sector.</Text>
                                    <Item inlineLabel style={{ marginTop: 30 }}>
                                        <Label style={{ color: 'rgba(0,0,0,.6)', fontSize: 18 }}>Empresa</Label>
                                        <Input style={{height: 40, borderColor: 'rgba(0,0,0,.4)', borderWidth: 1, color: 'rgba(0,0,0,.6)'}}
                                            returnKeyType="done"
                                            onChangeText={(empresa)=> this.setState({empresaPrompt: empresa})}
                                        />
                                    </Item>
                    </Col>
                    </Row>
                </Grid>       
            </View>
            <Divider style={{ backgroundColor: '#ddd', width: window.width - 20 }} />
            <View style={styles.modalContent}>
                <Grid style={{ alignItems: 'center', justifyContent: 'center', alignContent: 'center' }}>
                    <Col style={ styles.columnas }>
                            <TouchableOpacity
                             onPress={()=> {this._hideModal(); 
                                    this.setState({empresaPrompt: null})}}>
                            <Text style={{ color: 'rgba(0,0,0,.7)', textAlign: 'center', fontSize: 20 }}>Cancelar</Text>
                        </TouchableOpacity>
                    </Col>
                    <Col style={ styles.columnas }>
                            <TouchableOpacity
                             onPress={()=>  this.createEmpresa(this.state.empresaPrompt) }>
                            <Text style={{ color: 'rgba(0,0,0,.7)', textAlign: 'center', fontSize: 20 }}>Aceptar</Text>
                        </TouchableOpacity>
                    </Col>
                </Grid>
            </View>
        </Modal>
        </Content>
        {this.state.isActionButtonVisible ? this.getFab() : null} 
          <Display keepAlive={true} enter={'fadeIn'} exit={'fadeOut'}  enable={!this.state.timePassed} style={{position: 'absolute', zIndex: 100, width: window.width, height: window.height}}>
           <BlurView tint="dark" intensity={100} style={{position: 'absolute', zIndex: 100, width: window.width, height: window.height}} >
            <View style={{ flex: 1, justifyContent: 'center', alignContent: 'center' }}>
      <View
        style={{
           justifyContent: 'center', alignContent: 'center',width: window.width , marginLeft: window.width/2-100
        }}
      >
        <Animation
          style={{
            width:200,
            height:200 ,
          }}
          source={require('../assets/data12.json')}
          loop
         
          ref={(c)=> this.animation = c}
        />
      </View>
                     <Text style={{textAlign: 'center', color:'white', width: window.width}}>Cargando...</Text>
 

    </View>  
      </BlurView>
          </Display>
      </Container>
      
    );
  }
}

var styles = StyleSheet.create({
    headerText: {
        fontSize: 22,
        fontWeight: '100',
        color: '#fff',
        textAlign: 'center',
        marginTop: 8
    },
    headerJob: {
        fontSize: 16,
        fontWeight: '100',
        color: '#fff',
        textAlign: 'center'
    },
    headerTile: {
        fontSize: 16,
        fontWeight: '100',
        color: 'rgba(0,0,0,.6)',
        marginTop: 15,
        textAlign: 'center'
    },
    columnas: {
        borderWidth: 1, 
        borderColor: 'rgba(0,0,0,.1)',
        backgroundColor: 'rgba(255,255,255,1)',  
        justifyContent: 'center', 
        alignItems: 'center',
        height: 120
    },
    profile: {
        justifyContent: 'center', 
        alignItems: 'center', 
        marginTop: 22
    },
    actionButtonIcon: {
        height: 22,
        color: 'white',
    },
    container: {
        flex: 1,
        padding: 10
    },
    backgroundImage: {
        width: null,
        height: 300,
    },
    overlay: {
        position: 'absolute',
        top: 0,
        right: 0,
        bottom: 0,
        left: 0,
        backgroundColor: 'red',
        opacity: 0.3
    },
    modal: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    modal4: {
        height: 300
    },
    text: {
        color: "black",
        fontSize: 22
    },
    btn: {
        margin: 10,
        backgroundColor: "#3B5998",
        color: "white",
        padding: 10
    },

    btnModal: {
        position: "absolute",
        top: 0,
        right: 0,
        width: 50,
        height: 50,
        backgroundColor: "transparent"
    },
    button: {
        backgroundColor: 'lightblue',
        padding: 12,
        margin: 16,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        borderColor: 'rgba(0, 0, 0, 0.1)',
    },
    modalHeaderProspecto: {
        backgroundColor: '#fcfcfc',
        padding: 10,
        borderColor: 'rgba(0, 0, 0, 0.1)',
        height: 350
    },
    modalHeaderVinculo: {
        backgroundColor: 'rgba(189,9,38,1)',
        padding: 10,
        borderColor: 'rgba(0, 0, 0, 0.1)',
        height: 350
    },
    modalAvatar: {
        alignItems: 'center',
        flex: 1
    },
    modalContent: {
        backgroundColor: 'rgba(255,255,255,1)',
        height: 70,
        borderColor: 'rgba(0, 0, 0, 0.1)',
        alignItems: 'center', 
        justifyContent: 'center'
    },
    bottomModal: {
        justifyContent: 'flex-end',
        margin: 0,
    }
});
AppRegistry.registerComponent("Empresas", ()=> Empresas);

