import React from 'react';
import {
  AppRegistry,
  ScrollView, View, TouchableOpacity,
  Platform,
 Alert,
 AsyncStorage,
  Dimensions
} from 'react-native';
import { Button ,Form, Switch , Item, Label, Input ,  Container, Content, List, Separator  , ListItem, Left, Body, Right, Thumbnail, Text, Icon  } from 'native-base';
import { Avatar} from 'react-native-elements';
import ActionButton from 'react-native-action-button';
import { ImagePicker, Components, BlurView } from 'expo';
import Animation from 'lottie-react-native';
import TimerMixin from 'react-timer-mixin';

import Display from 'react-native-display';
let window = Dimensions.get('window')
import { StackNavigator } from 'react-navigation';

export default class editEmpresa extends React.Component{
  constructor(props){
  super(props);
  this.timeouts = [];
  this.animation = null;
  const {setParams} = this.props.navigation;
  this.navigation = this.props.navigation;
  this.state = {
   nombreBack: "Selecciona un Prospecto",
   apellidoBack: null,
   maternoBack: null,
   idProspecto: null,
   navigation: this.props.navigation,
    timePassed: false,
        nombre_empresa: null,
        nombreTitular: null,
        acuerdo: null,
        idUsuario: this.navigation.state.params.idUsuario,
        idEmpresa: this.navigation.state.params.idEmpresa,
        idSector: this.navigation.state.params.idSector
  }
}



static navigationOptions = {
    title: 'Datos de la Empresa',
    headerTintColor: '#FFF',
    
     headerStyle: {
        backgroundColor: '#41413f'
    },
  };

   focusNext(nextInput){
      nextInput._root.focus();
  }


animationPlay(){
  this.setState({timePassed: true});
  this.animation.play();
   this.timeouts.push(setTimeout(() => {
    this.setState({ timePassed: false });
    console.log("return")
    this.props.navigation.goBack();
  }, 3250));
}

updateEmpresa(nombre, nombreTitular, acuerdo ){
  console.log('//////// Inside createVinculo N' + nombre);
  console.log('//////// Inside createVinculo NV' + nombreTitular);
  console.log('//////// Inside createVinculo AP' + acuerdo);
if ( nombre == null || nombre == ''){
Alert.alert(
  'Error',
  'Como mínimo debes de llenar el campo de nombre de la empresa',
  [
    {text: 'Cancel', onPress: ()=> {}},
    {text: 'OK', onPress: ()=> { }}
  ],
  { cancelable: false }
)
}
else{
    fetch('http://innovatio.mx/reactAcem/updateEmpresa.php',{
            method: 'POST',
            header: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                idEmpresa: this.state.idEmpresa,
                idSector: this.state.idSector,
                idUsuario: this.state.idUsuario,
                nombre_empresa: nombre,
                nombreTitular: nombreTitular,
                acuerdo: acuerdo
            })
        }).then(( response )=> response.json())
        .then(( responseJSON )=> { 
              if(responseJSON.result == 'fallo'){
                Alert.alert(
                  'Error en la captura',
                  'Revise que tengas conexión a internet o que esten correctamente llenados los campos',
                  [
                    {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                    {text: 'OK', onPress: () => console.log('OK Pressed')},
                  ],
                  { cancelable: false }
                )
              }
              else{
                    this.animationPlay();
              }
         })
        .catch((error) => {
        console.error(error);
      });
}
}
    render(){
        return(
              <Container>
        <Content >
          <Form style={{backgroundColor: 'white'}}>
            <Separator bordered>
                <Text style={{ fontSize: 16 }} >Datos de la Empresa</Text>
            </Separator>
            <Item inlineLabel>
              <Label>Nombre de la empresa</Label>
              <Input
             returnKeyType="next"
               onSubmitEditing={()=> this.focusNext(this._pat)}
               onChangeText={(nombre)=> this.setState({nombre_empresa: nombre})} />
            </Item>
            <Item inlineLabel>
              <Label 
             
              >Nombre del Titular</Label>
              <Input 
              ref={(i) =>  this._pat = i}
               returnKeyType='next'
              onSubmitEditing={()=>this.focusNext(this._mat)}
              onChangeText={(nombreTitular)=> this.setState({nombreTitular: nombreTitular})}/>
            </Item>
            <Item inlineLabel>
              <Label>Acuerdo</Label>
              <Input ref={(i)=> this._mat = i} 
               returnKeyType={'done'}
               keyboardType="numeric"
              onChangeText={(acuerdo)=> this.setState({acuerdo: acuerdo})}/>
            </Item>
          </Form>
           <Button onPress={()=>{this.updateEmpresa(this.state.nombre_empresa, this.state.nombreTitular, this.state.acuerdo)}} block rounded style={{marginTop: 15, marginLeft: 20, marginRight: 20 , backgroundColor: '#41413f'}}>
            <Text>Editar Empresa</Text>
          </Button>
        </Content>
         <Display keepAlive={true} enter={'fadeIn'} exit={'fadeOut'}  enable={this.state.timePassed} style={{position: 'absolute', zIndex: 100, width: window.width, height: window.height}}>
           <BlurView tint="dark" intensity={100} style={{position: 'absolute', zIndex: 100, width: window.width, height: window.height}} >
            <View style={{ flex: 1, justifyContent: 'center', alignContent: 'center' }}>
      <View
        style={{
           justifyContent: 'center', alignContent: 'center',width: window.width , marginLeft: window.width/2-100
        }}
      >
        <Animation
          style={{
            width:200,
            height:200 ,
          }}
          source={require('../assets/data12.json')}
          loop
         
          ref={(c)=> this.animation = c}
        />
      </View>
                     <Text style={{textAlign: 'center', color:'white', width: window.width}}>Cargando...</Text>
 

    </View>  
      </BlurView>
          </Display>
      </Container>
        );
    }
}



AppRegistry.registerComponent("editEmpresa", ()=>editEmpresa);
