import React from 'react';
import {
  AppRegistry,
  ScrollView, View, TouchableOpacity,
  Platform,
 Alert,
 AsyncStorage,
  Dimensions
} from 'react-native';
import {Button ,Form, Switch , Item, Label, Input ,  Container, Content, List, Separator  , ListItem, Left, Body, Right, Thumbnail, Text, Icon  } from 'native-base';
import { Avatar} from 'react-native-elements';
import ActionButton from 'react-native-action-button';
import { ImagePicker, Components, BlurView } from 'expo';
import Animation from 'lottie-react-native';
import TimerMixin from 'react-timer-mixin';

import Display from 'react-native-display';
let window = Dimensions.get('window')
import { StackNavigator } from 'react-navigation';

export default class createProspectoScreen extends React.Component{
  constructor(props){
  super(props);
  this.timeouts = [];
  this.animation = null;
  const {setParams} = this.props.navigation;
  this.navigation = this.props.navigation;
  this.state = {
   sector: "Sector",
   empresa: "Empresa",
   navigation: this.props.navigation,
    timePassed: false,
    nombre: null,
    apellido_pat: "",
    apellido_mat: "", 
    correo: "",
    telefono_oficina: "",
    celular: "",
    puesto: "",
    idProspecto: null, 
    buttonText: "Crear Prospecto"
  }
}



static navigationOptions = ({navigation}) => ({
    title: (navigation.state.params.title),
    headerTintColor: '#FFF',
    
     headerStyle: {
        backgroundColor: '#41413f'
    },
  });

   focusNext(nextInput){
      nextInput._root.focus();
  }


async getEmpresa(){
  const DEMO_TOKEN = await AsyncStorage.getItem('id_token');
    this.navigation.navigate('selectEmpresa', {
      onGoBack: (local) => this.refresh(local), id: this.navigation.state.params.id });
   
}

componentDidMount(){
  if(this.navigation.state.params.isEditing){
    console.log("Editing Prospecto");
    this.setState({nombre: this.navigation.state.params.nombre,
      apellido_pat: this.navigation.state.params.apellido_pat,
      apellido_mat: this.navigation.state.params.apellido_mat, 
      correo: this.navigation.state.params.correo,
      telefono_oficina: this.navigation.state.params.telefono_oficina,
      celular: this.navigation.state.params.celular,
      puesto: this.navigation.state.params.puesto,
      empresa: this.navigation.state.params.empresa,
      idProspecto: this.navigation.state.params.idProspecto,
      buttonText: "Editar Información"});
      console.log(this.state);

  }
}

refresh(local) {
  this.setState({empresa: local})
}

animationPlay(){
  this.setState({timePassed: true});
  this.animation.play();
   this.timeouts.push(setTimeout(() => {
    this.setState({ timePassed: false });
    console.log("return")
    this.returnProspecto();
  }, 3250));
}

createNewProspecto(nombre, apellido_pat, apellido_mat, empresa, puesto, correo, telefono_oficina, celular){
   fetch('http://innovatio.mx/reactAcem//createProspecto.php',{
            method: 'POST',
            header: {
                 'Accept': 'application/json',
               'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                id: this.navigation.state.params.id ,
                idProspecto: this.state.idProspecto,
                nombre: nombre,
                apellido_pat: apellido_pat,
                apellido_mat: apellido_mat,
                empresa: empresa,
                puesto: puesto,
                correo: correo,
                telefono_oficina: telefono_oficina,
                celular: celular,
            })
        }).then((response)=>response.json())
        .then((responseJSON)=> {this.animationPlay()} )
        .catch((error) => {
        console.error(error);
      });
}



    render(){

      
  
        return(
              <Container>
        <Content >
          <Form style={{backgroundColor: 'white'}}>
            <Separator bordered>
                <Text> Contacto</Text>
            </Separator>
            <Item inlineLabel>
              <Label>Nombre</Label>
              <Input
                            keyboardAppearance="dark"
              defaultValue={this.state.nombre}
             returnKeyType="next"
             onChangeText={(t)=> this.setState({nombre: t})}
               onSubmitEditing={()=> this.focusNext(this._pat)} />
            </Item>
            <Item inlineLabel>
              <Label 
             
              >Apellido Paterno</Label>
              <Input  
                            keyboardAppearance="dark"
                            defaultValue={this.state.apellido_pat}

              ref={(i) =>  this._pat = i}
               returnKeyType='next'
               onChangeText={(t)=> this.setState({apellido_pat: t})}
              onSubmitEditing={()=>this.focusNext(this._mat)}
/>
            </Item>
            <Item inlineLabel>
              <Label>Apellido Materno</Label>
              
              <Input ref={(i)=> this._mat = i}
                            keyboardAppearance="dark"
                            defaultValue={this.state.apellido_mat}
              onChangeText={(t)=> this.setState({apellido_mat: t})}
               returnKeyType={'next'}
              onSubmitEditing={()=>this.focusNext(this._mail)}/>
            </Item>
            <Separator bordered>
                <Text>Datos de contacto</Text>
            </Separator>
             <Item inlineLabel>
              <Label>Correo</Label>
              <Input ref={(i)=> this._mail = i} 
                            keyboardAppearance="dark"
                            defaultValue={this.state.correo}
              onChangeText={(t)=> this.setState({correo: t})}
              keyboardType={'email-address'}
               returnKeyType={'next'}
              onSubmitEditing={()=>this.focusNext(this._ofi)}
              />
            </Item>
             <Item inlineLabel>
              <Label>Teléfono oficina</Label>
              <Input ref={(i) => this._ofi = i}
                            keyboardAppearance="dark"
                            defaultValue={this.state.telefono_oficina}

              onChangeText={(t)=> this.setState({telefono_oficina: t})}
               returnKeyType={'next'}
               keyboardType={'phone-pad'}
                onSubmitEditing={()=>this.focusNext(this._cel)}
              />
            </Item>
            <Item style={{backgroundColor: 'white'}} inlineLabel>
              <Label>Celular</Label>
              <Input ref={(i)=> this._cel = i}
                            keyboardAppearance="dark"
                            defaultValue={this.state.celular}

                             keyboardType={'phone-pad'}

              onChangeText={(t)=> this.setState({celular: t})}
               returnKeyType={'next'}
              />
            </Item>
            <Separator bordered>
                <Text>{this.state.sector}</Text>
            </Separator>
            
             <ListItem onPress={()=> this.getEmpresa()} icon>
              <Left>
                    <Button style={{ backgroundColor: '#41413f' }}>
                <Icon active ios="ios-paper" android="md-paper"  />
              </Button>
              </Left>
              <Body> 
                <Text>{this.state.empresa}</Text>
              </Body>
              <Right>
                <Icon name="arrow-forward" />
              </Right>
            </ListItem>
            <Item style={{backgroundColor: 'white'}} inlineLabel>
              <Label>Puesto</Label>
              <Input ref={(i)=> this._pues = i}
                            keyboardAppearance="dark"
                            defaultValue={this.state.puesto}

              onChangeText={(t)=> this.setState({puesto: t})}
               returnKeyType={'next'}
              />
            </Item>
          </Form>

           <Button onPress={()=>this.createNewProspecto(this.state.nombre, this.state.apellido_pat, this.state.apellido_mat, this.state.empresa, this.state.puesto, this.state.correo, this.state.telefono_oficina, this.state.celular)} block rounded style={{marginTop: 15, marginLeft: 20, marginRight: 20 , backgroundColor: '#41413f'}}>
            <Text>{this.state.buttonText}</Text>
          </Button>



        </Content>
         <Display keepAlive={true} enter={'fadeIn'} exit={'fadeOut'}  enable={this.state.timePassed} style={{position: 'absolute', zIndex: 100, width: window.width, height: window.height}}>
           <BlurView tint="dark" intensity={100} style={{position: 'absolute', zIndex: 100, width: window.width, height: window.height}} >
            <View style={{ flex: 1, justifyContent: 'center', alignContent: 'center' }}>
      <View
        style={{
           justifyContent: 'center', alignContent: 'center',width: window.width , marginLeft: window.width/2-100
        }}
      >
        <Animation
          style={{
            width:200,
            height:200 ,
          }}
          source={require('../assets/data12.json')}
          loop
         
          ref={(c)=> this.animation = c}
        />
      </View>
                     <Text style={{textAlign: 'center', color:'white', width: window.width}}>Cargando...</Text>
 

    </View>  
      </BlurView>
          </Display>
      </Container>
        );
    }

async returnProspecto(){
      await AsyncStorage.setItem('id_token2', "001");
      var text = "";
      if(this.navigation.state.params.isEditing){
        text = "editado"
      }else{
        text = "creado"
      }
    this.props.navigation.state.params.onGoBack(text);
    this.props.navigation.goBack();
}

}



AppRegistry.registerComponent("createProspecto", ()=>createProspecto);