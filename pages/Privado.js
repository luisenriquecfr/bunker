import React, { Component } from 'react';
import { Container, Content, Button, List, ListItem, Left, Body, Right, Thumbnail } from 'native-base';
import { Avatar, Tile, Header, Card, Icon, Divider  } from 'react-native-elements'
import { Text, Dimensions, View, StyleSheet, TouchableOpacity, Image, AppRegistry, ScrollView, Platform, UIManager, LayoutAnimation } from 'react-native';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { StackNavigator } from 'react-navigation';
import ActionButton from 'react-native-action-button';
const empresas = [
  {
    name: 'Kaitensoft',
    avatar_url: 'K',
    subtitle: 'Some dummy text'
  },
  {
    name: 'Innovatio',
    avatar_url: 'I',
    subtitle: 'Some dummy text'
  },
  {
    name: 'SEMARNAT',
    avatar_url: 'S',
    subtitle: 'Some dummy text'
  },
  {
    name: 'CEDENA',
    avatar_url: 'C',
    subtitle: 'Some dummy text'
  },
  {
    name: 'Tata',
    avatar_url: 'T',
    subtitle: 'Some dummy text'
  },
  {
    name: 'Kool-Aid',
    avatar_url: 'K',
    subtitle: 'Some dummy text'
  },
  {
    name: 'Oriflame',
    avatar_url: 'O',
    subtitle: 'Some dummy text'
  },
  {
    name: 'Banamex',
    avatar_url: 'B',
    subtitle: 'Some dummy text'
  },
  {
    name: 'Santander',
    avatar_url: 'S',
    subtitle: 'Some dummy text'
  },
  {
    name: 'HSBC',
    avatar_url: 'H',
    subtitle: 'Some dummy text'
  },
  {
    name: 'SAKA',
    avatar_url: 'S',
    subtitle: 'Some dummy text'
  },
  {
    name: 'CONADE',
    avatar_url: 'C',
    subtitle: 'Some dummy text'
  },
  {
    name: 'Yoli',
    avatar_url: 'Y',
    subtitle: 'Some dummy text'
  },
  {
    name: 'Angular',
    avatar_url: 'A',
    subtitle: 'Some dummy text'
  }
];
UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);
export default class Privado extends Component {
constructor(props){
    super(props);
    this.navigation = this.props.navigation;
  this.state = {
    isActionButtonVisible: true 
  };
}
static navigationOptions = {
    title: 'Empresas Privadas',
    headerTintColor: '#FFF',
     headerStyle: {
        backgroundColor: 'rgba(189,9,38,1)'
    },
  };
_listViewOffset = 0


_onScroll = (event) => {
  // Simple fade-in / fade-out animation
  const CustomLayoutLinear = {
    duration: 300,
    create: { type: LayoutAnimation.Types.easeInEaseOut, property: LayoutAnimation.Properties.opacity },
    update: { type: LayoutAnimation.Types.easeInEaseOut, property: LayoutAnimation.Properties.opacity },
    delete: { type: LayoutAnimation.Types.easeInEaseOut, property: LayoutAnimation.Properties.opacity }
  }
  // Check if the user is scrolling up or down by confronting the new scroll position with your own one
  const currentOffset = event.nativeEvent.contentOffset.y
  const direction = (currentOffset > 0 && currentOffset > this._listViewOffset)
    ? 'down'
    : 'up'
  // If the user is scrolling down (and the action-button is still visible) hide it
  const isActionButtonVisible = direction === 'up'
  if (isActionButtonVisible !== this.state.isActionButtonVisible) {
    LayoutAnimation.configureNext(CustomLayoutLinear)
    this.setState({ isActionButtonVisible })
  }
  // Update your scroll position
  this._listViewOffset = currentOffset
}
getFab(){
    return (Platform.OS === 'android') ? <ActionButton buttonColor="#bd1e52"></ActionButton> : null 
}
  render() {
    return (
    <Container>
        <Content onScroll={this._onScroll} >
          <List>
            {
                empresas.map((l, i) => (
                    <ListItem onPress={()=>this.navigation.navigate('Vinculos')} avatar>
                    <Left>
                        <Avatar
                        medium
                        rounded
                        key={i}
                        title={l.avatar_url}
                        onPress={() => console.log("Works!")}
                        activeOpacity={0.7}
                        overlayContainerStyle={{backgroundColor: 'rgba(189,9,38,1)'}}
                        />
                    </Left>
                    <Body>
                        <Text>{l.name}</Text>
                        <Text note>{l.subtitle}</Text>
                    </Body>
                    </ListItem>
                ))
            }
          </List>
        </Content>
        {this.state.isActionButtonVisible ? this.getFab() : null} 
      </Container>
      
    );
  }
}
class FAB extends Component{
    render(){
        return(
            <View style={{ flex: 1 }}>
            <ActionButton buttonColor="rgba(231,76,60,1)">
            </ActionButton>
        </View>
        );
    }
}
Privado.navigationOptions = props =>{
 const { navigation } = props;
  const { state, setParams } = navigation;
  const { params } = state;

return{
    title: 'Empresas',
    headerTintColor: '#FFF',
    headerRight:  (Platform.OS === 'ios') ? <TouchableOpacity onPress={()=>navigation.navigate('crearVinculo')} ><Icon name='ios-add' style={{fontSize: 27, color: 'white', marginRight: 20}} /></TouchableOpacity> : null  
,
     headerStyle: {
        backgroundColor: 'rgba(189,9,38,2)'
    },
}
}
var styles = StyleSheet.create({
    headerText: {
        fontSize: 22,
        fontWeight: '100',
        color: '#fff',
        textAlign: 'center',
        marginTop: 8
    },
    headerJob: {
        fontSize: 16,
        fontWeight: '100',
        color: '#fff',
        textAlign: 'center'
    },
    headerTile: {
        fontSize: 20,
        fontWeight: '100',
        color: 'rgba(0,0,0,.6)',
        marginTop: 40,
        textAlign: 'center'
    },
    columnas: {
        borderWidth: 1, 
        borderColor: 'rgba(0,0,0,.1)',
        backgroundColor: '#fff',  
        justifyContent: 'center', 
        alignItems: 'center'
    },
    profile: {
        justifyContent: 'center', 
        alignItems: 'center', 
        marginTop: 22
    },
    actionButtonIcon: {
        height: 22,
        color: 'white',
    }
});
AppRegistry.registerComponent("Privado", ()=> Privado);