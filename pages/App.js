import React from 'react';
import { StyleSheet, View, Image, TextInput, Alert, Dimensions, AppRegistry, Platform, StatusBar, AsyncStorage } from 'react-native';
import { Video, LinearGradient, Expo, Font, Exponent, AppLoading, Fingerprint } from 'expo';
import { Container, Header, Icon, Title, Content, Button, Item, Label, Input, Body, Left, Right, Form, Text } from 'native-base';
import { Divider } from 'react-native-elements';
import { StackNavigator } from 'react-navigation';
import Registro from './Registro';
import Home from './Home';
import { Col, Row, Grid } from "react-native-easy-grid";
import Prospectos from './Prospectos';
import createProspecto from './createProspecto';
import selectEmpresa from './selectEmpresa';
import Tabs from './Tabs';
import Publico from './Publico';
import Privado from './Privado';
import Sector from './Sector';
import Minutas from './minutas';
import CreateMinuta from './createMinuta';
import Vinculos from './Vinculos';
import crearVinculo from './crearVinculo';
import VinculosProvados from './vinculosPrivados';
import Empresas from './Empresas';
import { ImagePicker, Components, BlurView, SecureStore } from 'expo';
import Animation from 'lottie-react-native';
import TimerMixin from 'react-timer-mixin';
import selectProspecto from './selectProspecto';
import minutaView from './minutaView';
import EditPerfilScreen from './editPerfil';
import EditEmpresa from './editEmpresa'
const fingerPrintUsage = null;
 const  MainScreen = ({ navigation }) => {

    var timePassed = false;

    function focusNext(nextInput) {
        this._passInput._root.focus();
    }
    var mailState;
    var state;

    async function checkLoginMethod(){
        if(await AsyncStorage.getItem("auth") != "null"){
            if(await AsyncStorage.getItem("auth") == "1"){
                var mail;
                await SecureStore.getItemAsync("mailFinger").then(
                    onfulfilled=>{
                     mail = (onfulfilled)
                    }
                 )
                var pass;
                await SecureStore.getItemAsync("passFinger").then(
                    onfulfilled=>{
                        pass = (onfulfilled)
                    }
                );
                console.log(mail)
                loginWithFinger(mail, pass)
                
            }else{
                console.log("No fingerprint")
            }
        }else{

        }
    }
    function loginWithFinger(mail, pass){
        Fingerprint.authenticateAsync().then( 
            response =>{
                if(response.success == true){
                    getUser(mail, pass)
                }
            }
        )

    }
     function getUser(mail, pass){
        fetch('http://innovatio.mx/reactAcem/getUser.php',{
            method: 'POST',
            header: {
                 'Accept': 'application/json',
               'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                correo: mail,
                pass: pass
            })
        }).then((response)=>response.json())
        .then((responseJSON)=> {manageLogin(responseJSON)} )
        .catch((error) => {
        console.error(error);
      });
     }

     function manageLogin(response){
          if(response.result == 'existe'){
                navigation.navigate('HomeScreen', {id: response.id ,nombre: response.nombre, apellido_pat: response.apellido_pat, apellido_mat: response.apellido_mat, puesto: response.puesto, empresa: response.empresa , correo: response.correo, url: response.image, pass: response.pass });
          }
          else{
              Alert.alert(
                  'Error',
                  'Correo o contraseña incorrecta',
                  [
                      {text: 'Aceptar'}
                  ]
              )
          }
      }

      async function   askForFingerPrint(mail, pass){
        if(await AsyncStorage.getItem("auth") == "1" && Fingerprint.hasHardwareAsync()){
            checkLoginMethod()

        }else if(Fingerprint.hasHardwareAsync()) {
            Alert.alert("¿Quieres usar tu huella digital para iniciar sesión?", null, 
            [
                { text: "Aceptar", onPress:  ()=> { AsyncStorage.setItem("auth", '1');
                 SecureStore.setItemAsync("mailFinger", mail);
                 SecureStore.setItemAsync("passFinger", pass);
                 getUser(mail, pass);
                 
            }},
                { text: "Cancelar", onPress: ()=> { AsyncStorage.setItem("auth", "null");
                getUser(mail, pass);
            }}
                
            ]
        )

        }else{
            getUser(mail,pass);
        }
      }

    

    return (
       
        <View
            style={{ flex: 1, alignContent: 'center', justifyContent: 'center', width: window.width, height: window.height }} style={styles.container}
        >
        <Image style={styles.backgroundVideo} source={require('../assets/fondo.jpg')} />  



            <Grid>

                <Row style={{ marginTop: 30, alignContent: 'center', justifyContent: 'center' }} size={1}>
                    <Logo />
                </Row>
                <Form>
                    <Item style={{ width: window.width - 30 }} inlineLabel>
                        <Label style={{ color: '#CFCABB'}}>Correo</Label>
                        <Input style={{ color: '#CFCABB' }}
                        keyboardAppearance="dark"
                            returnKeyType="next"
                            keyboardType="email-address"
                            onSubmitEditing={() => { focusNext(this._passInput); console.log(mailState) }}
                            onChangeText={(mail) => mailState = mail}
                        />
                    </Item>
                    <Item style={{ width: window.width - 30, }} inlineLabel>
                        <Label style={{ color: '#CFCABB' }}>Contraseña</Label>
                        <Input style={{ color: '#CFCABB' }}
                            keyboardAppearance="dark"
                            ref={c => this._passInput = c}
                            secureTextEntry={true}
                            returnKeyType="done"
                            onChangeText={(pass) => state = pass}
                            value={state}
                            onSubmitEditing={() => { console.log(state);  askForFingerPrint(mailState, state); }}
                        />
                    </Item>
                </Form>
                <Row style={{ alignContent: 'center', justifyContent: 'center' }} size={1}>
                    <Button onPress={() =>{  askForFingerPrint(mailState, state); }} block style={{ backgroundColor: '#CFCABB', margin: 15, marginTop: 20, width: window.width / 2 - 30 }}>
                        <Text style={{ color: 'black' }}>Iniciar Sesión</Text>
                    </Button>
                    <Button onPress={() => navigation.navigate('Registro')} ref={(c) => this._button = c} block style={{ backgroundColor: '#CFCABB', margin: 15, marginTop: 20, width: window.width / 2 - 30 }}>
                        <Text style={{ color: 'black' }}>Registro</Text>
                    </Button>
                </Row>
                
            </Grid>
            
        </View>

       
    

    );
}



class Main extends React.Component {


    state = {fontsAreLoaded:  false};
async componentWillMount() {    
        StatusBar.setBarStyle('light-content', true);
        StatusBar.setBackgroundColor('black', true);
        await  Font.loadAsync({
          'Roboto': require('native-base/Fonts/Roboto.ttf'),
          'Roboto_medium': require('native-base/Fonts/Roboto_medium.ttf'),
        }); 
        this.setState({fontsAreLoaded: true});
    }

    componentDidMount(){
        this.LognManager()
    }

    async LognManager(){
        try{
            console.log("mount")
             fingerPrintUsage = await AsyncStorage.getItem("auth");
            console.log(fingerPrintUsage);
            
        }catch(error){
            console.log ("Something Happened: " + error);
        }
        if(fingerPrintUsage == '1'){
            this.checkLoginMethod();    
        }
    }
     async checkLoginMethod(){
        if(fingerPrintUsage != "null"){
            if(fingerPrintUsage == "1"){
               var mail;
               await SecureStore.getItemAsync("mailFinger").then(
                   onfulfilled=>{
                    mail = (onfulfilled)
                   }
                )
               var pass;
               await SecureStore.getItemAsync("passFinger").then(
                   onfulfilled=>{
                       pass = (onfulfilled)
                   }
               );
                this.loginWithFinger(mail, pass)
            }else{
                console.log("No fingerprint")
            }
        }
    }
     loginWithFinger(mail, pass){
        Fingerprint.authenticateAsync().then( 
            response =>{
                if(response.success == true){
                    this.getUser(mail, pass)
                    console.log(mail, pass)
                }
            } 
        )

    }

     getUser(mail, pass){
        fetch('http://innovatio.mx/reactAcem/getUser.php',{
            method: 'POST',
            header: {
                 'Accept': 'application/json',
               'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                correo: mail,
                pass: pass
            })
        }).then((response)=>response.json())
        .then((responseJSON)=> {this.manageLogin(responseJSON)} )
        .catch((error) => {
        console.error(error);
      });
     }
    askForFingerPrintUsage(){
       if(Fingerprint.hasHardwareAsync){
           if(Fingerprint.isEnrolledAsync){

           }else{
               Alert.alert("Aviso", "No tienes huellas registradas")
           }
       }else{
           Alert.alert("Error", "Tu dispositivo no cuenta con lector de huellas")
       }
    }
     manageLogin(response){
        if(response.result == 'existe'){
              this.props.navigation.navigate('HomeScreen', {id: response.id ,nombre: response.nombre, apellido_pat: response.apellido_pat, apellido_mat: response.apellido_mat, puesto: response.puesto, empresa: response.empresa , correo: response.correo, url: response.image, pass: response.pass });
        }
        else{
            Alert.alert(
                'Error',
                'Correo o contraseña incorrecta',
                [
                    {text: 'Aceptar'}
                ]
            )
        }
    }
    static navigationOptions = {

        header: null
    };

    render() {
          if (!this.state.fontsAreLoaded) {
      return <AppLoading/>
    }
 
        return (
    
        <MainScreen navigation={this.props.navigation} />


        
    );

    
    }


}


const AppNavigator = StackNavigator(
    {
        Home: { screen: Main },
        Registro: { screen: Registro },
        HomeScreen: { screen: Home },
        Prospectos: {screen: Prospectos},
        createProspecto: {screen: createProspecto},
        selectEmpresa: {screen: selectEmpresa},
        Tabs: {screen: Tabs},
        Publico: {screen: Publico},
        Privado:{screen: Privado},
        Sector: {screen: Sector},
        Minutas: {screen: Minutas},
        CreateMinuta: {screen: CreateMinuta},
        Vinculos: {screen: Vinculos},
        crearVinculo: {screen: crearVinculo},
        Empresas: {screen: Empresas},
                selectProspecto: {screen: selectProspecto},
                minutaView: {screen: minutaView},
                EditPerfilScreen: {screen: EditPerfilScreen} ,
                editEmpresa: {screen: EditEmpresa},

        Index: {
            screen: Main,
        },

    },
    {
        initialRouteName: 'Index',
        mode: 'card',
        headerMode: 'float'
    }
);

export default () => <AppNavigator />





const Logo = ({ navigation }) => (
    <Image
        source={require('../assets/Logo_Bunker.png')}
        style={{ width: 225, height: 225 }}
    />

);


const window = Dimensions.get('window');

const styles = StyleSheet.create({
    container: {
        flex: 1,
        zIndex: 100,
    },
    backgroundVideo: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
        zIndex: -100
    },

});



