import React from 'react';
import {
  AppRegistry,
  Text,
  ScrollView, View, Dimensions, Alert
} from 'react-native';
import { Container, Content, Form, Item, Input, Label, Button, Separator } from 'native-base';
import { ImagePicker, Components, BlurView } from 'expo';
import Animation from 'lottie-react-native';
import TimerMixin from 'react-timer-mixin';

import Display from 'react-native-display';
let window = Dimensions.get('window')
import { StackNavigator } from 'react-navigation';

 export default class RegistroScreen extends React.Component {
constructor(props){
  super(props);
  this.timeouts = [];
  this.navigation = this.props.navigation;
  this.state = {
    timePassed: false,
    nombre: null,
    apellido_pat: null,
    apellido_mat: null,
    empresa: null,
    puesto: null,
    correo: null,
    pass: null,
  }
}

   focusNext(nextInput){
      nextInput._root.focus();
  }


animationPlay(){
  this.setState({timePassed: true});
  
  this.animation.play();
   this.timeouts.push(setTimeout(() => {
    this.setState({ timePassed: false });
    this.navigation.goBack();
  }, 3250));
}

  static navigationOptions = {
    title: 'Registro',
    headerTintColor: '#FFF',

     headerStyle: {
        backgroundColor: '#41413F'
    },
  };

   createUser(nombre, apellido_pat, apellido_mat, puesto, empresa, correo, pass){
     if ( nombre == null || apellido_pat == null || apellido_mat == null || puesto == null || empresa == null || correo == null || pass == null || nombre == '' || apellido_pat == '' || apellido_mat == '' || puesto == '' || empresa == '' || correo == '' || pass == '' ){
      Alert.alert(
        'Error',
        'Revisa que hayas llenado correctamente todos los campos',
        [
          {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
          {text: 'OK', onPress: () => console.log('OK Pressed')},
        ],
        { cancelable: false }
      )
     }
     else{
        fetch('http://innovatio.mx/reactAcem/createUser.php',{
            method: 'POST',
            header: {
                 'Accept': 'application/json',
               'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                nombre: nombre,
                apellido_pat: apellido_pat,
                apellido_mat: apellido_mat,
                empresa: empresa,
                puesto: puesto,
                correo: correo,
                password: pass
            })
        }).then((response)=>response.json())
        .then((responseJSON)=> {this.manageCreate(responseJSON)} )
        .catch((error) => {
        console.error(error);
      });
     }
     }

      manageCreate(response){
          if(response.result == 'exito'){
      Alert.alert(
        'Éxito',
        'Cuenta creada satisfactoriamente',
        [
          {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
          {text: 'OK', onPress: () => console.log('OK Pressed')},
        ],
        { cancelable: false }
      )
                this.animationPlay();
          }
          else{
              Alert.alert(
                  'Error',
                  'Ocurrio un error, intenta más tarde',
                  [
                      {text: 'Aceptar'}
                  ]
              )
          }
      }



  render() {
    return (
    
   <Container>
        <Content >
          <Form style={{backgroundColor: 'white'}}>
                <Separator bordered>
                <Text>Datos de Personales</Text>
            </Separator>
            <Item inlineLabel>
              <Label>Nombre</Label>
              <Input 
              returnKeyType="next"
              onSubmitEditing={()=> this.focusNext(this._pat)}
              onChangeText={(name)=> this.setState({nombre: name})}
                            keyboardAppearance="dark"

              />
              
            </Item>
            <Item inlineLabel >
              <Label>Apellido Paterno</Label>
              <Input 
              ref={(i) => this._pat = i}
              returnKeyType="next"
              onSubmitEditing={()=> this.focusNext(this._mat)}
              onChangeText={(pat)=> this.setState({apellido_pat: pat})}
              keyboardAppearance="dark"

              />
            </Item>
            <Item inlineLabel>
              <Label>Apellido Materno</Label>
              <Input
               ref={(i) => this._mat = i}
               onSubmitEditing={()=> this.focusNext(this._emp)}
              onChangeText={(mat)=> this.setState({apellido_mat: mat})}
              keyboardAppearance="dark"

              returnKeyType="next"/>
            </Item>
           
             <Separator bordered>
                <Text>Datos de cuenta</Text>
            </Separator>
            <Item inlineLabel>

              <Label>Empresa</Label>
              <Input
              ref={(i) => this._emp = i}
               onSubmitEditing={()=> this.focusNext(this._pues)}
              onChangeText={(emp)=> this.setState({empresa: emp})}
              keyboardAppearance="dark"

              returnKeyType="next"/>
            </Item>
             <Item inlineLabel>
              <Label>Puesto</Label>
              <Input
              ref={(i) => this._pues = i}
              onChangeText={(pues)=> this.setState({puesto: pues})}
              onSubmitEditing={()=> this.focusNext(this._mail)}
                            keyboardAppearance="dark"

              returnKeyType="next"/>
            </Item>
            <Item inlineLabel>
              <Label>Correo</Label>
              <Input
              ref={(i) => this._mail = i}
               onSubmitEditing={()=> this.focusNext(this._pass)}
               keyboardType="email-address"
                             keyboardAppearance="dark"

              onChangeText={(mail)=> this.setState({correo: mail})}
              returnKeyType="next"/>
            </Item>
            <Item inlineLabel>
              <Label>Contraseña</Label>
              <Input
              ref={(i) => this._pass = i}
              keyboardAppearance="dark"
               onChangeText={(pass)=> this.setState({pass: pass})}

              secureTextEntry={true}
              returnKeyType="next"/>
            </Item>
          </Form>
          
           <Button onPress={()=>{this.createUser(this.state.nombre, this.state.apellido_pat, this.state.apellido_mat, this.state.puesto, this.state.empresa, this.state.correo, this.state.pass)}} block rounded style={{marginTop: 15, marginLeft: 20, marginRight: 20 , backgroundColor: '#41413F'}}>
            <Text style={{color: 'white'}}>Crear cuenta</Text>
          </Button>

        </Content>
<Display keepAlive={true} enter={'fadeIn'} exit={'fadeOut'}  enable={this.state.timePassed} style={{position: 'absolute', zIndex: 100, width: window.width, height: window.height}}>
           <BlurView tint="dark" intensity={100} style={{position: 'absolute', zIndex: 100, width: window.width, height: window.height}} >
            <View style={{ flex: 1, justifyContent: 'center', alignContent: 'center' }}>
      <View
        style={{
           justifyContent: 'center', alignContent: 'center',width: window.width , marginLeft: window.width/2-100
        }}
      >
        <Animation
          style={{
            width:200,
            height:200 ,
          }}
          source={require('../assets/data12.json')}
          loop
         
          ref={(c)=> this.animation = c}
        />
      </View>
                     <Text style={{textAlign: 'center', color:'white', width: window.width}}>Cargando...</Text>
 

    </View>  
      </BlurView>
          </Display>

      </Container>);
  }
}

const Registro = StackNavigator({
  Registro: {screen: RegistroScreen}
});

AppRegistry.registerComponent('Registro', () => Registro);
