import React from 'react';
import {
  AppRegistry,
  ScrollView, View, TouchableOpacity,
  Platform,
 Alert,
 AsyncStorage, WebView, Dimensions,
 TouchableNativeFeedback
} from 'react-native';
import {ImagePicker, Components, BlurView} from 'expo';
import Display from 'react-native-display';
import {Button ,Form, Switch , Item, Label, Input ,  Container, Content, List, Separator  , ListItem, Left, Body, Right, Thumbnail, Text, Icon  } from 'native-base';
import { Avatar} from 'react-native-elements';
import ActionButton from 'react-native-action-button';
let window = Dimensions.get('window')
import Animation from 'lottie-react-native';

import { StackNavigator } from 'react-navigation';

export default class createMinutasScreen extends React.Component{
    constructor(props){
  super(props);
  
  this.navigation = this.props.navigation;
  
this.timeouts = [];
  this.state = {
      url: "",
      loaded: false,
            timePassed: false,

  }


  
}

 static navigationOptions = ({navigation}) => {
    const {
      params = {}
    } = navigation.state
    const {
      nav = {}
    } = navigation;

    return {
      title: 'Minuta',
      headerTintColor: '#FFF', 
     
      headerStyle: {
        backgroundColor: '#41413f'
      },
      headerLeft:  <TouchableOpacity onPress={()=> params.handleBack()}><Icon  ios="ios-arrow-back" android="md-arrow-back" style={{color: 'white', marginLeft: 20}} /></TouchableOpacity>      
    }
  };


componentWillMount(){
    let uri = "http://innovatio.mx/reactAcem/editMinuta.php?id=" + this.navigation.state.params.id + "&idMinuta=" + this.navigation.state.params.idMinuta;
    let uri2 = "http://innovatio.mx/reactAcem/editMinuta.php?id=" + this.navigation.state.params.id ;


    if (this.navigation.state.params.idMinuta == null){
            this.setState({url: uri2 });
            console.log(uri2);
    }else{
            this.setState({url: uri });
            console.log(uri);
    }

}

componentDidMount(){
    this.animation.play();
     this
      .timeouts
      .push(setTimeout(() => {
        this.setState({timePassed: true});
        console.log(this.state.prospectosArr);

      }, 3000));


      this.navigation.setParams({handleBack: this.returnMinuta.bind(this)})
}

onMessage( event ) {
Alert.alert("Minuta Guardada", "La minuta fue guardada de manera exitosa en el servidor",
  [
    {title: 'Aceptar', onPress: ()=>this.returnMinuta()},
  ]
);

 }


async returnMinuta(){
        await AsyncStorage.setItem('id_token2', "001");
          this.props.navigation.state.params.onGoBack();
    this.props.navigation.goBack();

}

  onLoad(e) {
    // onload is called multiple times...
    if ( this.state.loaded == true ) {
      return null
    }
    this.setState({ loaded: true }, () => this.bridge.injectJavaScript('window.onLoad()'))
  }

    render(){

const patchPostMessageFunction = function() {
  var originalPostMessage = window.postMessage;

  var patchedPostMessage = function(message, targetOrigin, transfer) { 
    originalPostMessage(message, targetOrigin, transfer);
  };

  patchedPostMessage.toString = function() { 
    return String(Object.hasOwnProperty).replace('hasOwnProperty', 'postMessage');
  };

  window.postMessage = patchedPostMessage;
};

const patchPostMessageJsCode = '(' + String(patchPostMessageFunction) + ')();';


        return(
            <View            style={{ height: window.height, width: window.width}}
>
           <WebView
           style={{ height: window.height, width: window.width}}
           onLoad={this.onLoad.bind(this)}
            ref={n => this.bridge = n} 
                        javaScriptEnabled={true} 

           injectedJavaScript={patchPostMessageJsCode}
        onMessage={this.state.loaded ? this.onMessage.bind(this) : null} 
            source={{uri: this.state.url}}/>

                  <Display
          enter={'fadeIn'}
          exit={'fadeOut'}
          enable={!this.state.timePassed || !this.state.loaded}
          style={{
          position: 'absolute',
          zIndex: 100000,
          width: window.width,
          height: window.height
        }}>
          <BlurView
            tint="dark"
            intensity={100}
            style={{
            position: 'absolute',
            zIndex: 100,
            width: window.width,
            height: window.height - 50
          }}>
            <View
              style={{
              flex: 1,
              justifyContent: 'center',
              alignContent: 'center'
            }}>
              <View
                style={{
                justifyContent: 'center',
                alignContent: 'center',
                width: window.width,
                marginLeft: window.width / 2 - 100
              }}>
                <Animation
                  style={{
                  width: 200,
                  height: 200
                }}
                  source={require('../assets/data12.json')}
                  loop
                  ref={(c) => this.animation = c}/>
              </View>
              <Text
                style={{
                textAlign: 'center',
                color: 'white',
                width: window.width
              }}>Cargando...</Text>

            </View>
          </BlurView>
        </Display>
            </View>

            
        );
    }
} 

AppRegistry.registerComponent("CreateMinuta", ()=>CreateMinuta);