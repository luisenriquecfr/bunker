import React from 'react';
import {
  AppRegistry,
  ScrollView, View, TouchableOpacity,
  Platform,
 Alert,
 AsyncStorage,
  Dimensions
} from 'react-native';
import { Button ,Form, Switch , Item, Label, Input ,  Container, Content, List, Separator  , ListItem, Left, Body, Right, Thumbnail, Text, Icon  } from 'native-base';
import { Avatar} from 'react-native-elements';
import ActionButton from 'react-native-action-button';
import { ImagePicker, Components, BlurView } from 'expo';
import Animation from 'lottie-react-native';
import TimerMixin from 'react-timer-mixin';

import Display from 'react-native-display';
let window = Dimensions.get('window')
import { StackNavigator } from 'react-navigation';

export default class crearVinculo extends React.Component{
  constructor(props){
  super(props);
  this.timeouts = [];
  this.animation = null;
  const {setParams} = this.props.navigation;
  this.navigation = this.props.navigation;
  this.state = {
   nombreBack: "Selecciona un Prospecto",
   apellidoBack: null,
   maternoBack: null,
   idProspecto: null,
   navigation: this.props.navigation,
    timePassed: false,
        nombre_vinculo: null,
        nombre_enlace: null,
        apellido_pat_vinculo: null,
        apellido_mat_vinculo: null,
        correo_vinculo: null,
        telefono_vinculo: null,
        puesto_vinculo: null,
        empresa_vinculo: null,
        idUsuario: this.navigation.state.params.idUsuario,
        idRed: this.navigation.state.params.idRed,
        update: this.navigation.state.params.update,
        idVinculo: this.navigation.state.params.idVinculo
  }
}



static navigationOptions = {
    title: 'Crear',
    headerTintColor: '#FFF',
    
     headerStyle: {
        backgroundColor: '#41413f'
    },
  };

   focusNext(nextInput){
      nextInput._root.focus();
  }


/**async getProspecto(){
  const DEMO_TOKEN = await AsyncStorage.getItem('id_token');
    this.navigation.navigate('selectProspecto', {
      onGoBack: (id, nombre, apellido_pat, apellido_mat) => this.backParamas(id, nombre, apellido_pat, apellido_mat), idUsuario: this.navigation.state.params.idUsuario });
      console.log('///// Inside goback Crear' + id )
      console.log('/////// Inside goback Crear' + nombre )
      console.log('//////// Inside goback Crear' + apellido_pat )
      console.log('//////// Inside goback Crear' + apellido_mat )
   
}**/

/** backParamas(id, nombre, apellido_pat, apellido_mat) {
  this.setState({ nombreBack: nombre });
  this.setState({ apellidoBack: apellido_pat });
  this.setState({ maternoBack: apellido_mat });
  this.setState({ idProspecto: id });
}**/

animationPlay(){
  this.setState({timePassed: true});
  this.animation.play();
   this.timeouts.push(setTimeout(() => {
    this.setState({ timePassed: false });
    console.log("return")
    this.props.navigation.goBack();
  }, 3250));
}

createVinculo(nombre, apellido_pat, apellido_mat, correo, telefono, puesto, empresa, nombre_vinculo){
  console.log('//////// Inside createVinculo N' + nombre);
  console.log('//////// Inside createVinculo NV' + nombre_vinculo);
  console.log('//////// Inside createVinculo AP' + apellido_pat);
  console.log('//////// Inside createVinculo AM' + apellido_mat);
  console.log('//////// Inside createVinculo C' + correo);
  console.log('//////// Inside createVinculo P' + puesto);
  console.log('//////// Inside createVinculo E' + empresa);
  console.log('//////// Inside createVinculo T' + telefono);
  console.log('//////// Inside createVinculo Red' + this.state.idRed);
  console.log('//////// Inside createVinculo Usuairo' + this.state.idUsuario);
  console.log('//////// Inside createVinculo Vinculo' + this.state.idVinculo);
  if( this.state.update == 'true' ){
    if ( nombre == null || nombre == ''){
Alert.alert(
  'Error',
  'Revisa que hayas llenado correctamente todos los campos',
  [
    {text: 'Cancel', onPress: ()=> {}},
    {text: 'OK', onPress: ()=> { }}
  ],
  { cancelable: false }
)
}
else{
    fetch('http://innovatio.mx/reactAcem/updateVinculos.php',{
            method: 'POST',
            header: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                idVinculo: this.state.idVinculo,
                nombre_vinculo: nombre,
                apellido_pat_vinculo: apellido_pat,
                apellido_mat_vinculo: apellido_mat,
                correo_vinculo: correo,
                puesto_vinculo: puesto,
                empresa_vinculo: empresa,
                telefono_vinculo: telefono,
                nombre_enlace: nombre_vinculo
            })
        }).then(( response )=> response.json())
        .then(( responseJSON )=> { 
              if(responseJSON.result == 'fallo'){
                Alert.alert(
                  'Error en la captura',
                  'Revise que tengas conexión a internet o que esten correctamente llenados los campos',
                  [
                    {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                    {text: 'OK', onPress: () => console.log('OK Pressed')},
                  ],
                  { cancelable: false }
                )
              }
              else{
                    this.animationPlay();
              }
         })
        .catch((error) => {
        console.error(error);
      });
}
    console.log('Edit');
  }
  else{
if ( nombre == null || nombre == '' || nombre_vinculo == null || nombre_vinculo == ''){
Alert.alert(
  'Error',
  'Revisa que hayas llenado correctamente todos los campos',
  [
    {text: 'Cancel', onPress: ()=> {}},
    {text: 'OK', onPress: ()=> { }}
  ],
  { cancelable: false }
)
}
else{
    fetch('http://innovatio.mx/reactAcem/createVinculo.php',{
            method: 'POST',
            header: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                idUsuario: this.state.idUsuario,
                idProspecto: this.state.idProspecto,
                idRed: this.state.idRed,
                nombre_vinculo: nombre,
                apellido_pat_vinculo: apellido_pat,
                apellido_mat_vinculo: apellido_mat,
                correo_vinculo: correo,
                puesto_vinculo: puesto,
                empresa_vinculo: empresa,
                telefono_vinculo: telefono,
                nombre_enlace: nombre_vinculo
            })
        }).then(( response )=> response.json())
        .then(( responseJSON )=> { 
              if(responseJSON.result == 'fallo'){
                Alert.alert(
                  'Error en la captura',
                  'Revise que tengas conexión a internet o que esten correctamente llenados los campos',
                  [
                    {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                    {text: 'OK', onPress: () => console.log('OK Pressed')},
                  ],
                  { cancelable: false }
                )
              }
              else{
                    this.animationPlay();
              }
         })
        .catch((error) => {
        console.error(error);
      });
}
    console.log('Post');
  }
}
    render(){
        return(
              <Container>
        <Content >
          <Form style={{backgroundColor: 'white'}}>
            <Separator bordered>
                <Text style={{ fontSize: 16 }} >Datos del Titular de la Depedencia</Text>
            </Separator>
            <Item inlineLabel>
              <Label>Nombre</Label>
              <Input
             returnKeyType="next"
               onSubmitEditing={()=> this.focusNext(this._pat)}
               onChangeText={(nombre)=> this.setState({nombre_vinculo: nombre})} />
            </Item>
            <Item inlineLabel>
              <Label 
             
              >Apellido Paterno</Label>
              <Input 
              ref={(i) =>  this._pat = i}
               returnKeyType='next'
              onSubmitEditing={()=>this.focusNext(this._mat)}
              onChangeText={(paterno)=> this.setState({apellido_pat_vinculo: paterno})}/>
            </Item>
            <Item inlineLabel>
              <Label>Apellido Materno</Label>
              
              <Input ref={(i)=> this._mat = i} 
               returnKeyType={'next'}
              onSubmitEditing={()=>this.focusNext(this._mail)}
              onChangeText={(materno)=> this.setState({apellido_mat_vinculo: materno})}/>
            </Item>
             <Item inlineLabel>
              <Label>Correo</Label>
              <Input style={{ height: 80 }} ref={(i)=> this._mail = i} 
               returnKeyType={'next'}
               keyboardType="email-address"
              onSubmitEditing={()=>this.focusNext(this._ofi)}
              onChangeText={(correo)=> this.setState({correo_vinculo: correo})}
              />
            </Item>
             <Item inlineLabel>
              <Label>Teléfono</Label>
              <Input ref={(i) => this._ofi = i}
               returnKeyType={'next'}
               keyboardType="phone-pad"
                onSubmitEditing={()=>this.focusNext(this._puesto)}
                onChangeText={(telefono)=> this.setState({telefono_vinculo: telefono})}
              />
            </Item>
            <Item style={{backgroundColor: 'white'}} inlineLabel>
              <Label>Puesto</Label>
              <Input ref={(i)=> this._puesto = i}
               returnKeyType={'next'}
               onSubmitEditing={()=>this.focusNext(this._empresa)}
               onChangeText={(puesto)=> this.setState({puesto_vinculo: puesto})}
              />
            </Item>
            <Item style={{backgroundColor: 'white'}} inlineLabel>
              <Label>Empresa</Label>
              <Input ref={(i)=> this._empresa = i}
              onChangeText={(empresa)=> this.setState({empresa_vinculo: empresa})}/>
            </Item>
            <Separator bordered>
                <Text style={{ fontSize: 16 }} >Datos del Vinculo</Text>
            </Separator>
            <Item inlineLabel>
              <Label>Nombre del Vinculo</Label>
              <Input
               onChangeText={(nombre_vinculo)=> this.setState({nombre_enlace: nombre_vinculo})} />
            </Item>
          </Form>
           <Button onPress={()=>{this.createVinculo(this.state.nombre_vinculo, this.state.apellido_pat_vinculo, this.state.apellido_mat_vinculo, this.state.correo_vinculo, this.state.telefono_vinculo, this.state.puesto_vinculo, this.state.empresa_vinculo, this.state.nombre_enlace)}} block rounded style={{marginTop: 15, marginLeft: 20, marginRight: 20 , backgroundColor: '#41413f'}}>
            <Text>Crear Vinculo</Text>
          </Button>
        </Content>
         <Display keepAlive={true} enter={'fadeIn'} exit={'fadeOut'}  enable={this.state.timePassed} style={{position: 'absolute', zIndex: 100, width: window.width, height: window.height}}>
           <BlurView tint="dark" intensity={100} style={{position: 'absolute', zIndex: 100, width: window.width, height: window.height}} >
            <View style={{ flex: 1, justifyContent: 'center', alignContent: 'center' }}>
      <View
        style={{
           justifyContent: 'center', alignContent: 'center',width: window.width , marginLeft: window.width/2-100
        }}
      >
        <Animation
          style={{
            width:200,
            height:200 ,
          }}
          source={require('../assets/data12.json')}
          loop
         
          ref={(c)=> this.animation = c}
        />
      </View>
                     <Text style={{textAlign: 'center', color:'white', width: window.width}}>Cargando...</Text>
 

    </View>  
      </BlurView>
          </Display>
      </Container>
        );
    }
}



AppRegistry.registerComponent("crearVinculo", ()=>crearVinculo);
