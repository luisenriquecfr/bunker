import React, { Component } from 'react';
import { Container , Content, List, ListItem, Left, Body, Right, Thumbnail, CardItem, Button, Card, Input, Form, Item, Label, Icon } from 'native-base';
import { Avatar, Tile, Header, Divider  } from 'react-native-elements'
import { Text, Dimensions, View, StyleSheet, Platform , TouchableOpacity, Image, AppRegistry, ScrollView, UIManager, LayoutAnimation, Alert, TextInput, AlertIOS } from 'react-native'
import { Col, Row, Grid } from 'react-native-easy-grid';
import { StackNavigator } from 'react-navigation';
import ActionButton from 'react-native-action-button';
import { LinearGradient } from 'expo';
import { ImagePicker, Components, BlurView } from 'expo';
import Animation from 'lottie-react-native';
import TimerMixin from 'react-timer-mixin';
import Display from 'react-native-display';
import Modal from 'react-native-modal';
let window = Dimensions.get('window');
UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);
export default class Sector extends Component {
constructor(props){
    super(props);
    this.navigation = this.props.navigation;
    this.params = this.props.navigation.state;
    this.timeouts = [];
    this.animation = null;
    this.sectores = [];
    this.sectoresUsuario = [];
     this.state = {
        navigation: this.props.navigation,
        timePassed: false,
        idUsuario: this.navigation.state.params.idUsuario,
        isActionButtonVisible: true,
        isModalVisible: false,
        sectorPrompt: null
  }
  console.log('/////// IdUsuario en Sector' + this.state.idUsuario);
}

static navigationOptions  = ({navigation}) => {
    const { params = {} } = navigation.state
    const { nav = {} } = navigation;
    return{
    title: 'Alianzas',
    headerTintColor: '#FFF',
    headerRight:  (Platform.OS === 'ios' ) ? <TouchableOpacity onPress={()=>{params.handleSave()}}
 ><Icon type='ionicon' name='ios-add'  color='white' size={35} style={{ marginRight: 20, color: 'white'}} /></TouchableOpacity> : null,
     headerStyle: {
        backgroundColor: '#41413f'
    }, 
}
}

 componentDidMount() {
this.props.navigation.setParams({ handleSave: this.showPrompt.bind(this) });
    fetch('http://innovatio.mx/reactAcem/getAlianzas.php',{
        method: 'POST',
        header: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                idUsuario: this.state.idUsuario
            })
        }).then((response)=> response.json())
        .then(( responseJSON )=> { 
              if(responseJSON.result == 'vacio'){

              }
              else{
            this.sectores = responseJSON
            console.log('//// Inside SectoresUsuario Data Usuario' + this.sectores);
              }
         })
        .catch((error) => {
        console.error(error);
      });
    this.animation.play();
    this.timeouts.push(setTimeout(() => {
        this.setState({ timePassed: true });
    }, 3250));
}

fetchSectores(){
     fetch('http://innovatio.mx/reactAcem/getAlianzas.php',{
        method: 'POST',
        header: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                idUsuario: this.state.idUsuario
            })
        }).then((response)=> response.json())
        .then(( responseJSON )=> { 
              if(responseJSON.result == 'vacio'){

              }
              else{
            this.sectores = responseJSON
            console.log('//// Inside SectoresUsuario Data Usuario' + this.sectores);
              }
         })
        .catch((error) => {
        console.error(error);
      });
}


clearTimeouts(){
  this.timeouts.forEach(clearTimeout);
}
  
componentWillUnmount(){
  this.clearTimeouts();
}

  _showModal = () => this.setState({ isModalVisible: true })

  _hideModal = () => this.setState({ isModalVisible: false })



_listViewOffset = 0


_onScroll = (event) => {
  // Simple fade-in / fade-out animation
  const CustomLayoutLinear = {
    duration: 300,
    create: { type: LayoutAnimation.Types.easeInEaseOut, property: LayoutAnimation.Properties.opacity },
    update: { type: LayoutAnimation.Types.easeInEaseOut, property: LayoutAnimation.Properties.opacity },
    delete: { type: LayoutAnimation.Types.easeInEaseOut, property: LayoutAnimation.Properties.opacity }
  }
  // Check if the user is scrolling up or down by confronting the new scroll position with your own one
  const currentOffset = event.nativeEvent.contentOffset.y
  const direction = (currentOffset > 0 && currentOffset > this._listViewOffset)
    ? 'down'
    : 'up'
  // If the user is scrolling down (and the action-button is still visible) hide it
  const isActionButtonVisible = direction === 'up'
  if (isActionButtonVisible !== this.state.isActionButtonVisible) {
    LayoutAnimation.configureNext(CustomLayoutLinear)
    this.setState({ isActionButtonVisible })
  }
  // Update your scroll position
  this._listViewOffset = currentOffset
}

getFab(){
    return (Platform.OS === 'android') ? <ActionButton onPress={() => this._showModal() } buttonTextStyle={{ color:'#41413f' }}  buttonColor="#fff"></ActionButton> : null 
}

 showPrompt(){
    AlertIOS.prompt(
  'Crear nueva empresa',
  'Introduce el nombre de la nueva empresa',[
    {text: 'Cancelar', onPress: (text)=> {this.setState({sectorPrompt: null})} },
    {text: 'Aceptar', onPress: (text)=> {console.log(text); this.setState({sectorPrompt: text}); this.createSector(this.state.sectorPrompt) } }
  ]
);
    }

createSector(sector){
if ( sector == null || sector == '' ){
Alert.alert(
  'Error',
  'Revisa que hayas llenado correctamente el nombre del sector',
  [
    {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
    {text: 'OK', onPress: () => console.log('OK Pressed')},
  ],
  { cancelable: false }
)
}
else{
this._hideModal();
console.log('Inside Create Sector Nombre Empresa' + sector);
console.log('//// Create Sector Usuario' + this.state.idUsuario);
    fetch('http://innovatio.mx/reactAcem/createAlianzas.php',{
        method: 'POST',
        header: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                idUsuario: this.state.idUsuario,
                nombre_sector: sector
            })
        }).then((response)=> response.json())
        .then(( responseJSON )=> { 
              if(responseJSON.result == 'vacio'){
                Alert.alert(
                'Error',
                'Revisa que tengas conexión a internet o vuelve a intentar en un momento',
                [
                    {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                    {text: 'OK', onPress: () => console.log('OK Pressed')},
                ],
                { cancelable: false }
                )
              }
              else{
                this.fetchSectores();
                this.forceUpdate();


                Alert.alert(
                'Éxito',
                'El Sector de ha agregado exitosamente',
                [
                    {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                    {text: 'OK', onPress: () => console.log('OK Pressed')},
                ],
                { cancelable: false }
                )
              }
              this.setState({sectorPrompt: null})
         })
        .catch((error) => {
        console.error(error);
      });
}
}

getImage(){

    if(Platform.OS === "ios"){
        return  <Image   
                    resizeMode="cover"
                    style={{ position: 'absolute',
                    width: window.width-35,
                    height: 180, 
                    
                    
                    }} source={require('../assets/bunker/alianza_negro.png')}/>
    } else{
        return  <Image   
                    resizeMode="cover"
                    style={{ position: 'absolute',
                    width: window.width-20,
                    height: 170
                    }} source={require('../assets/bunker/alianza_negro.png')}/>
    }


}

  render() {
    return (
        <Container style={{ flex: 1 }} >
            <Content onScroll={this._onScroll}>
                <List>
            {
                this.sectores.map((s, i) => (
                <ListItem style={{paddingRight: 15, marginBottom: 20}}  onPress={() => this.navigation.navigate('Empresas', {idSector: s.id, nombre_sector: s.nombre_sector, idUsuario: this.state.idUsuario})}> 
                <Card style={{ padding: 10 }} key={i}>
                {this.getImage()}
                    <Grid>
                        <Row style={{ backgroundColor: 'rgba(255,255,255,.0)', height: 70}}>
                            <Col>
                                <Text style={{ width: window.width/2 , fontSize: 20, color: '#fff', textAlign: 'left', fontWeight: 'bold' }}> {s.nombre_sector}</Text>
                            </Col>
                        </Row>
                    </Grid>
                    <Grid>
                        <Row style={{ backgroundColor: 'rgba(255,255,255,.0)', height: 80 }}>
                            <Col style={{ backgroundColor: 'rgba(255,255,255,.0)'}}>
                        <Row size={2} style={{ flexDirection: 'row', marginTop: 10, alignItems: 'center' }} >
                            <Text style={{ textAlign: 'left', fontSize: 16, color: '#fff', backgroundColor: 'transparent' }} >Ir a Sector   </Text>
                            <Icon style={{fontSize: 22, color: '#cfcabb', justifyContent: 'flex-end'}} ios="ios-arrow-forward" android="md-arrow-forward" color='#cfcabb'>
                            </Icon>
                        </Row>
                       
                    </Col>
                        <Col style={{ backgroundColor: 'rgba(255,255,255,.0)'}}>
                        </Col>
                    </Row>
                </Grid>
            </Card>
            </ListItem>
                ))
            }
            </List>
        <Modal isVisible={this.state.isModalVisible}>
            <View style={styles.modalHeaderProspecto}>
                <Grid>
                    <Row size={1}>
                    <Col style={{ backgroundColor: 'rgba(255,255,255,.0)', height: 30 }} >
                            <TouchableOpacity
                             onPress={()=> {this._hideModal(); 
                                    this.setState({sectorPrompt: null})}}>
                            <Icon style={{fontSize: 35, color: 'rgba(0,0,0,.6)', justifyContent: 'flex-end'}} ios="ios-close" android="md-close" color='rgba(0,0,0,.6)'>
                            </Icon>
                        </TouchableOpacity>
                    </Col>
                    <Col style={{ backgroundColor: 'rgba(255,255,255,.0)', height: 30, alignItems: 'flex-end' }} >
                    </Col>
                    </Row>
                    <Row size={4}>
                    <Col style={{ alignItems: 'center', alignContent: 'center', justifyContent: 'center' }} >
                        <Icon style={{fontSize: 88, color: 'rgba(0,0,0,.6)', alignItems: 'center', justifyContent: 'center'}} ios="ios-briefcase" android="md-briefcase" color='rgba(0,0,0,.6)'>
                            </Icon>
                                <Text style={{ fontSize: 18, fontWeight: '100', color: 'rgba(0,0,0,.6)', textAlign: 'center', justifyContent: 'center', alignItems: 'center', marginTop: 30 }} >
                                Añade el nombre del Sector que quieres agregar a la lista.</Text>
                                    <Item inlineLabel style={{ marginTop: 30, borderBottomWidth: 0 }}>
                                        <Label style={{ color: 'rgba(0,0,0,.6)', fontSize: 18 }}>Sector</Label>
                                        <Input style={{height: 40, borderColor: 'rgba(0,0,0,.4)', borderWidth: 1, color: 'rgba(0,0,0,.6)'}}
                                            returnKeyType="done"
                                            onChangeText={(sector)=> this.setState({sectorPrompt: sector})}
                                        />
                                    </Item>
                    </Col>
                    </Row>
                </Grid>       
            </View>
            <Divider style={{ backgroundColor: '#ddd', width: window.width - 20 }} />
            <View style={styles.modalContent}>
                <Grid style={{ alignItems: 'center', justifyContent: 'center', alignContent: 'center' }}>
                    <Col style={ styles.columnas }>
                            <TouchableOpacity
                             onPress={()=> {this._hideModal(); 
                                    this.setState({sectorPrompt: null})}}>
                            <Text style={{ color: 'rgba(0,0,0,.7)', textAlign: 'center', fontSize: 20 }}>Cancelar</Text>
                        </TouchableOpacity>
                    </Col>
                    <Col style={ styles.columnas }>
                            <TouchableOpacity
                             onPress={()=>  this.createSector(this.state.sectorPrompt) }>
                            <Text style={{ color: 'rgba(0,0,0,.7)', textAlign: 'center', fontSize: 20 }}>Aceptar</Text>
                        </TouchableOpacity>
                    </Col>
                </Grid>
            </View>
        </Modal>
        </Content>
            {this.state.isActionButtonVisible ? this.getFab() : null} 
             <Display keepAlive={true} enter={'fadeIn'} exit={'fadeOut'}  enable={!this.state.timePassed} style={{position: 'absolute', zIndex: 100, width: window.width, height: window.height}}>
                <BlurView tint="dark" intensity={100} style={{position: 'absolute', zIndex: 100, width: window.width, height: window.height}} >
                    <View style={{ flex: 1, justifyContent: 'center', alignContent: 'center' }}>
                        <View
                            style={{
                            justifyContent: 'center', alignContent: 'center',width: window.width , marginLeft: window.width/2-100
                            }}>
                        <Animation
                        style={{
                            width:200,
                            height:200 ,
                        }}
                        source={require('../assets/data12.json')}
                        loop
                        ref={(c)=> this.animation = c}
                        />
                        </View>
                        <Text style={{ fontSize: 24, textAlign: 'center', color:'white', width: window.width }}>Cargando...</Text>
                    </View>  
                </BlurView>
          </Display>
        </Container>
    );
  }
}

var styles = StyleSheet.create({
    headerText: {
        fontSize: 22,
        fontWeight: '100',
        color: '#fff',
        textAlign: 'center',
        marginTop: 8
    },
    headerJob: {
        fontSize: 16,
        fontWeight: '100',
        color: '#fff',
        textAlign: 'center'
    },
    headerTile: {
        fontSize: 16,
        fontWeight: '100',
        color: 'rgba(0,0,0,.6)',
        marginTop: 15,
        textAlign: 'center'
    },
    columnas: {
        borderWidth: 1, 
        borderColor: 'rgba(0,0,0,.1)',
        backgroundColor: 'rgba(255,255,255,1)',  
        justifyContent: 'center', 
        alignItems: 'center',
        height: 120
    },
    profile: {
        justifyContent: 'center', 
        alignItems: 'center', 
        marginTop: 22
    },
    actionButtonIcon: {
        height: 22,
        color: 'white',
    },
    container: {
        flex: 1,
        padding: 10
    },
    backgroundImage: {
        width: null,
        height: 300,
    },
    overlay: {
        position: 'absolute',
        top: 0,
        right: 0,
        bottom: 0,
        left: 0,
        backgroundColor: 'red',
        opacity: 0.3
    },
    modal: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    modal4: {
        height: 300
    },
    text: {
        color: "black",
        fontSize: 22
    },
    btn: {
        margin: 10,
        backgroundColor: "#3B5998",
        color: "white",
        padding: 10
    },

    btnModal: {
        position: "absolute",
        top: 0,
        right: 0,
        width: 50,
        height: 50,
        backgroundColor: "transparent"
    },
    button: {
        backgroundColor: 'lightblue',
        padding: 12,
        margin: 16,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        borderColor: 'rgba(0, 0, 0, 0.1)',
    },
    modalHeaderProspecto: {
        backgroundColor: '#fcfcfc',
        padding: 10,
        borderColor: 'rgba(0, 0, 0, 0.1)',
        height: 350
    },
    modalHeaderVinculo: {
        backgroundColor: 'rgba(189,9,38,1)',
        padding: 10,
        borderColor: 'rgba(0, 0, 0, 0.1)',
        height: 350
    },
    modalAvatar: {
        alignItems: 'center',
        flex: 1
    },
    modalContent: {
        backgroundColor: 'rgba(255,255,255,1)',
        height: 70,
        borderColor: 'rgba(0, 0, 0, 0.1)',
        alignItems: 'center', 
        justifyContent: 'center'
    },
    bottomModal: {
        justifyContent: 'flex-end',
        margin: 0,
    }
});


AppRegistry.registerComponent("Sector", ()=> Sector);