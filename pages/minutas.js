import React from 'react';
import {
    AppRegistry,
    ScrollView,
    View,
    TouchableOpacity,
    Platform,
    StyleSheet,
    LayoutAnimation, 
    AsyncStorage,
    Dimensions, 
    Alert
} from 'react-native';
import RNFetchBlob from 'react-native-fetch-blob'
import Share, {ShareSheet, Button} from 'react-native-share';
import { ImagePicker, Components, WebBrowser, BlurView } from 'expo';
import Animation from 'lottie-react-native';
import TimerMixin from 'react-timer-mixin';

import Display from 'react-native-display';
let window = Dimensions.get('window')


import {
    Container,
    Content,
    List,
    ListItem,
    Left,
    Body,
    Right,
    Thumbnail,
    Text,
    Icon
} from 'native-base';
import {Avatar} from 'react-native-elements';
import ActionButton from 'react-native-action-button';

import {StackNavigator} from 'react-navigation';
console.ignoredYellowBox = ['Warning: Failed prop type'];

const minutas = [
    {
        title: "Minuta 1 ",
        subtitle: "Subtitulo 1",
        fecha: "30-06-17",
        hora: "13:21",
        avatar: "MS"
    }
]

export default class minutasScreen extends React.Component {
    constructor(props) {
        super(props);
        this.swipeList = [];
        this.navigation = this.props.navigation;
            this.params = this.props.navigation.state;
            this.minutas = [];
        this.state = {
            text: "",
            isActionButtonVisible: true,
            swipeList: this.swipeList,
            sectionID: null,
            scrollEnabled: true,
            scroll: null,
            rowID: null, 
            minutas: [],
            visible: false,
            timePassed: false,
        }
    }

 onCancel() {
    console.log("CANCEL")
    this.setState({visible:false});
  }
  onOpen() {
    console.log("OPEN")
    this.setState({visible:true});
  }

static navigationOptions = ({navigation}) => {
    const {
      params = {}
    } = navigation.state
    const {
      nav = {}
    } = navigation;

    return {
      title: 'Minutas',
      headerTintColor: '#FFF',
      headerRight: (Platform.OS === 'ios') ?
         <TouchableOpacity onPress={() => params.createMinuta(navigation, params)}><Icon
            name='ios-add'
            style={{
            fontSize: 27,
            color: 'white',
            marginRight: 20
          }}/></TouchableOpacity>: null,
        
      headerStyle: {
        backgroundColor: '#41413f'
      }
    }
  };

getProspectos(id) {
    fetch('http://innovatio.mx/reactAcem/getMinutas.php', {
      method: 'POST',
      header: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({id: this.navigation.state.params.id})
    }).then((response) => response.json()).then((responseJSON) => {
      if(responseJSON.result != 'vacio'){
          this.minutas = responseJSON;
          this.setState({timePassed: true});
      this.setState({dataLoaded: true}
      )}else{
          console.log("sin minutas");
          this.setState({timePassed: true});
      }
      
    }).catch((error) => {
      console.error(error);
    });
  }

    componentDidMount() {
        this.props.navigation.setParams({createMinuta: this.createNewMinuta.bind(this), handleSave: this.saveDetails.bind(this)});
        this.getProspectos(this.navigation.state.params.id);
        this.animation.play();
}
saveDetails() {

    this.getProspectos(this.navigation.state.params.id);
    this.forceUpdate();

  }
  
downloadPDF(id, minutaID){

    

    let url = 'http://www.innovatio.mx/reactAcem/pdfGenerator.php?id='+ id + "&idMinuta=" + minutaID;  
this.animation.play();



    RNFetchBlob
  .config({
    fileCache : true,
    // by adding this option, the temp files will have a file extension
    appendExt : 'pdf'
  })
  .fetch('GET', url, {
    //some headers ..
    'Content-Type': 'application/PDF'
  })
  .then((res) => {

    let shareOptions = {
      title: "Enviar PDF",
      message: "pdf",
      url:  Platform.OS === 'android' ? 'file://' + res.path()  : '' + res.path(),
      subject: "Minuta" //  for email
    };
    this.setState({timePassed: true});
              Share.open(shareOptions);


    // the temp file path with file extension `png`
    console.log('The file saved to ', res.path())
    // Beware that when using a file path as Image source on Android,
    // you must prepend "file://"" before the file path
  })
}

async createNewMinuta(navigation, params) {
    const DEMO_TOKEN = await AsyncStorage.getItem('id_token2');
    navigation.navigate('CreateMinuta', {
      onGoBack: (state) => params.handleSave(state),
      id: navigation.state.params.id,
      idMinuta: null
    });
  }


async handleView(id, idMinuta){
        let url = 'http://www.innovatio.mx/reactAcem/pdfGenerator.php?id='+ id + "&idMinuta=" + idMinuta;  

    let result = await WebBrowser.openBrowserAsync(url);
}
 deleteMinuta(id){
    fetch('http://innovatio.mx/reactAcem/deleteMinuta.php',{
      method: 'POST',
      header: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        id: this.navigation.state.params.id,
        idMinuta: id
      })
    }).then((response) =>  response.json()).then((responseJSON) => {
            Alert.alert("Minuta borrada exitosamente");

      this.getProspectos(this.navigation.state.params.id);
      this.forceUpdate();
      
    }).catch((error) => {
      console.error(error);
    }); 
  }

async createNewMinuta2(navigation, params, idMinuta) {
    const DEMO_TOKEN = await AsyncStorage.getItem('id_token2');
    navigation.navigate('CreateMinuta', {
      onGoBack: (state) => this.refreshView(),
      id: navigation.state.params.id,
      idMinuta: idMinuta
    });
  }


refreshView(){
    this.getProspectos(this.navigation.state.params.id);
    this.forceUpdate();
}

    initSwipes() {
        for (let i in minutas) {
            this
                .swipeList
                .push(false);
        }

        this.setState({swipeList: this.swipeList});
    }

    toggleSwipes(index) {
        for (let i in minutas) {
            if (i = index) {
                this.swipeList[i] = true;
            } else {
                this.swipeList[i] = false;
            }
        }

        this.setState({swipeList: this.swipeList})
    }

    _listViewOffset = 0

    _onScroll = (event) => {
        // Simple fade-in / fade-out animation
        const CustomLayoutLinear = {
            duration: 100,
            create: {
                type: LayoutAnimation.Types.linear,
                property: LayoutAnimation.Properties.opacity
            },
            update: {
                type: LayoutAnimation.Types.linear,
                property: LayoutAnimation.Properties.opacity
            },
            delete: {
                type: LayoutAnimation.Types.linear,
                property: LayoutAnimation.Properties.opacity
            }
        }
        // Check if the user is scrolling up or down by confronting the new scroll
        // position with your own one
        const currentOffset = event.nativeEvent.contentOffset.y
        const direction = (currentOffset > 0 && currentOffset > this._listViewOffset)
            ? 'down'
            : 'up'
        // If the user is scrolling down (and the action-button is still visible) hide
        // it
        const isActionButtonVisible = direction === 'up'
        if (isActionButtonVisible !== this.state.isActionButtonVisible) {
            LayoutAnimation.configureNext(CustomLayoutLinear)
            this.setState({isActionButtonVisible})
        }
        // Update your scroll position
        this._listViewOffset = currentOffset
    }
    getFab() {
        return (Platform.OS === 'android')
            ? <ActionButton
                    onPress={() => this.createNewMinuta2(this.navigation, this.navigation.state)}
                   buttonTextStyle={{ color: '#41413f' }} buttonColor="#fff"></ActionButton>
            : null
    }

    render() {
        Swipeout = require('react-native-swipeout').default;
       
        return (
            <Container>
                <Content scroll={this.state.scroll}>
                    {(this.minutas).map((l, i) => (
                        <Swipeout
                            key={"swipe" + i}
                            style={{backgroundColor: 'white'}}
                            right={[
                            {
                                text: <Icon
                                    style={{
                                    fontSize: 27,
                                    color: 'white'
                                }}
                                    ios='ios-download-outline'
                                    android='md-download'
                                    color='white'></Icon>,
                                backgroundColor: 'rgb(76,217,100)',
                                onPress: ()=> {this.handleView(this.navigation.state.params.id, l.id)}
                            }, {
                                text: <Icon
                                    style={{
                                    fontSize: 27,
                                    color: 'white'
                                }}
                                    ios='ios-send-outline'
                                    android='md-send'
                                    color='white'></Icon>,
                                backgroundColor: 'rgb(0,122,255)',
                                onPress: ()=> {this.downloadPDF(this.navigation.state.params.id, l.id); this.setState({timePassed: false}); }
                            }, {
                                text: <Icon
                                    style={{
                                    fontSize: 27,
                                    color: 'white'
                                }}
                                    ios='ios-trash-outline'
                                    android='md-trash'
                                    color='white'></Icon>,
                                backgroundColor: 'rgb(255,59,48)',
                                onPress: ()=> this.deleteMinuta(l.id)
                            }
                        ]}>
                            <ListItem
                                style={{
                                backgroundColor: 'white'
                            }}
                                key={i + "List"}
                                onPress={() => this.createNewMinuta2(this.navigation, this.navigation.state.params, l.id)}
                                avatar>

                                <Left key={i + "Left"}>
                                    <Avatar
                                        small
                                        key={i + "Avatar"}
                                        rounded
                                        title={(l.titulo).substring(0,2)}
                                        overlayContainerStyle={{
                                        backgroundColor: '#cfcabb'
                                    }}
                                        onPress={() => console.log("Works!")}
                                        activeOpacity={0.7}/>
                                </Left>
                                <Body key={i + "Body"}>

                                    <Text key={i + "text"}>{l.titulo}</Text>
                                    <Text note  key={i + "date"}>
                                       Creación:  {l.subtitulo}
                                    </Text>

                                </Body>
                                <Right key={i + "right"}>
                                    
 <Text note key={i + "subtitle"}>Ultima Modificación: {"\n"}{l.fecha}</Text>

                                </Right>
                            </ListItem>
                        </Swipeout>
                    ))}

                   
                </Content>


                 <Display keepAlive={true} enter={'fadeIn'} exit={'fadeOut'}  enable={!this.state.timePassed} style={{position: 'absolute', zIndex: 100, width: window.width, height: window.height}}>
           <BlurView tint="dark" intensity={100} style={{position: 'absolute', zIndex: 100, width: window.width, height: window.height}} >
            <View style={{ flex: 1, justifyContent: 'center', alignContent: 'center' }}>
      <View
        style={{
           justifyContent: 'center', alignContent: 'center',width: window.width , marginLeft: window.width/2-100
        }}
      >
        <Animation
          style={{
            width:200,
            height:200 ,
          }}
          source={require('../assets/data12.json')}
          loop
         
          ref={(c)=> this.animation= c}
        />
      </View>
                     <Text style={{textAlign: 'center', color:'white', width: window.width}}>Cargando...</Text>
 

    </View>  
      </BlurView>
          </Display>


                 {this.state.isActionButtonVisible
                        ? this.getFab()
                        : null}
            </Container>

        );
    }
}

async function createNewMinuta(navigation, params) {
    const DEMO_TOKEN = await AsyncStorage.getItem('id_token2');
    navigation.navigate('CreateMinuta', {
      onGoBack: (state) => params.handleSave(state),
      id: navigation.state.params.id,
      idMinuta: null
    });
  }

AppRegistry.registerComponent("Minutas", () => Minutas);