import React, { Component } from 'react';
import { AppRegistry, AlertIOS, Platform, TouchableOpacity, AsyncStorage, Animated, Dimensions, View, StyleSheet } from 'react-native';
import {Radio , Text ,  Container , Content, List, ListItem, Left, Body, Right, Thumbnail, CardItem, Button, Card, Input, Form, Item, Label, Icon } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { Avatar, Tile, Header, Divider  } from 'react-native-elements'

import Animation from 'lottie-react-native';
import TimerMixin from 'react-timer-mixin';
import { ImagePicker, Components , BlurView} from 'expo';
import ActionButton from 'react-native-action-button';
import Modal from 'react-native-modal';
import Display from 'react-native-display';
let window = Dimensions.get('window')

export default class PickerEmpresa extends React.Component {
constructor(props){
    super(props);
    this.navigation = this.props.navigation;
    this.promptResult;
    this.empresasList = [];
      this.timeouts = [];
    this.state = {
   
    timePassed: false,
      empresas: this.empresasList,
      empresasArr: [],
       dataLoaded: false,
              isActionButtonVisible: true,
              isModalVisible: false,
              sectorPrompt: null

    };
}

_listViewOffset = 0;


_onScroll = (event) => {
  // Simple fade-in / fade-out animation
  const CustomLayoutLinear = {
    duration: 100,
    create: { type: LayoutAnimation.Types.linear, property: LayoutAnimation.Properties.opacity },
    update: { type: LayoutAnimation.Types.linear, property: LayoutAnimation.Properties.opacity },
    delete: { type: LayoutAnimation.Types.linear, property: LayoutAnimation.Properties.opacity }
  }
  // Check if the user is scrolling up or down by confronting the new scroll position with your own one
  const currentOffset = event.nativeEvent.contentOffset.y
  const direction = (currentOffset > 0 && currentOffset > this._listViewOffset)
    ? 'down'
    : 'up'
  // If the user is scrolling down (and the action-button is still visible) hide it
  const isActionButtonVisible = direction === 'up'
  if (isActionButtonVisible !== this.state.isActionButtonVisible) {
    LayoutAnimation.configureNext(CustomLayoutLinear)
    this.setState({ isActionButtonVisible })
  }
  // Update your scroll position
  this._listViewOffset = currentOffset

}


getEmpresas(id){
        fetch('http://innovatio.mx/reactAcem/getEmpresasProspectos.php',{
            method: 'POST',
            header: {
                 'Accept': 'application/json',
               'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                id: id,
            })
        }).then((response)=>response.json())
        .then((responseJSON)=> {
        if(responseJSON.result != 'vacio'){
        
      this.setState({empresasArr:  responseJSON});
      this.setState({dataLoaded: true});
      console.log(responseJSON);
      this.initRadios();
    }
      else{
        this.setState({dataLoaded: true});
       console.log("Sin empresas");
      }
    
    })
        .catch((error) => {
        console.error(error);
      });
     }

initRadios(){
  for(let i in this.state.empresasArr){
    this.empresasList.push(false);
  }
console.log(this.empresasList);
  this.setState({empresas: this.empresasList});
}

 toggleRadio(index) {
   console.log(index.i);
   console.log(this.empresasList);
      for(let i in this.empresasList){
        console.log(i);
        if(i == index.i){
          this.empresasList[i] = true;
        }else{
          this.empresasList[i] = false;
        }
      }
      this.setState({empresas: this.empresasList});
      console.log(this.state.empresas);
  }

getFab =  (
   <ActionButton onPress={()=>this.setState({isModalVisible: true})} buttonTextStyle={{ color: '#41413f' }} buttonColor="#fff"></ActionButton> 
 );
getButton= (
   <Button block onPress={()=>  this.returnEmpresa() } rounded style={{marginBottom: 20, marginLeft: 20, marginRight: 20 , backgroundColor: '#41413f'}}>
            <Text style={{ color:'#fff' }}>Aceptar</Text>
  </Button>
);
managePlatform(){
  return (Platform.OS === 'ios') ? this.getButton : this.getFab
}
 showPrompt(){
    AlertIOS.prompt(
  'Crear nueva empresa',
  'Introduce el nombre de la nueva empresa',
  text => console.log("m"+text)
);}
static navigationOptions  = ({navigation}) => {
    const { params = {} } = navigation.state
    const { nav = {} } = navigation;
    return{
    title: 'Selecciona una empresa',
   
    headerTintColor: '#FFF',
    headerRight:  (Platform.OS === 'ios') ? <TouchableOpacity onPress={()=>params.handleSave2()}
 ><Icon name='ios-add'  style={{fontSize: 27, color: 'white', marginRight: 20}} /></TouchableOpacity> : 
<TouchableOpacity onPress={()=>params.handleSave()} ><Text  style={{fontSize: 18, color: 'white', marginRight: 20}}> Aceptar </Text></TouchableOpacity>  
,
     headerStyle: {
        backgroundColor: '#41413f'
    }, 
}
}

componentDidMount(){
    this.props.navigation.setParams({ handleSave: this.returnEmpresa.bind(this), handleSave2: this.showPrompt.bind(this) });
this.getEmpresas( this.navigation.state.params.id);

this.animation.play();
 this.timeouts.push(setTimeout(() => {
    this.setState({ timePassed: true });
  }, 3000));
}


clearTimeouts(){
  this.timeouts.forEach(clearTimeout);
}
  
componentWillUnmount(){
  this.clearTimeouts();
}

_hideModal(){
  this.setState({isModalVisible: false});
}


render(){
      const {setParams} = this.props.navigation;
      if(!this.state.timePassed || !this.state.dataLoaded){
         return (
                  <Display  enter={'fadeIn'} exit={'fadeOut'}  enable={!this.state.timePassed} style={{position: 'absolute', zIndex: 100000, width: window.width, height: window.height}}>
           <BlurView tint="dark" intensity={100} style={{position: 'absolute', zIndex: 100, width: window.width, height: window.height-50}} >
            <View style={{ flex: 1, justifyContent: 'center', alignContent: 'center' }}>
      <View
        style={{
           justifyContent: 'center', alignContent: 'center',width: window.width , marginLeft: window.width/2-100
        }}
      >
        <Animation
          style={{
            width:200,
            height:200 ,
          }}
          source={require('../assets/data12.json')}
          loop
         
          ref={(c)=> this.animation= c}
        />
      </View>
                     <Text style={{textAlign: 'center', color:'white', width: window.width}}>Cargando...</Text>
 

    </View>  
      </BlurView>
          </Display>);
       }

    return(
        <Container>
        <Content>
          {(this.state.empresasArr).map((l,i) => (
          <ListItem key={i + "Item"} onPress={()=>this.toggleRadio({i})} selected={this.state.empresas[i]}>
            <Text key={i + "Text"}>{l.empresa}</Text>
            <Right key={i + "Right"}>
              <Radio key={i + "Radio"} onPress={()=>this.toggleRadio({i})} selected={this.state.empresas[i]}  />
            </Right>
          </ListItem>
          ))}

  <Modal isVisible={this.state.isModalVisible}>
            <View style={styles.modalHeaderProspecto}>
                <Grid>
                    <Row size={1}>
                    <Col style={{ backgroundColor: 'rgba(255,255,255,.0)', height: 30 }} >
                            <TouchableOpacity
                             onPress={()=> {this._hideModal(); 
                                    this.setState({sectorPrompt: null})}}>
                            <Icon style={{fontSize: 35, color: 'rgba(0,0,0,.6)', justifyContent: 'flex-end'}} ios="ios-close" android="md-close" color='rgba(0,0,0,.6)'>
                            </Icon>
                        </TouchableOpacity>
                    </Col>
                    <Col style={{ backgroundColor: 'rgba(255,255,255,.0)', height: 30, alignItems: 'flex-end' }} >
                    </Col>
                    </Row>
                    <Row size={4}>
                    <Col style={{ alignItems: 'center', alignContent: 'center', justifyContent: 'center' }} >
                        <Icon style={{fontSize: 88, color: 'rgba(0,0,0,.6)', alignItems: 'center', justifyContent: 'center'}} ios="ios-briefcase" android="md-briefcase" color='rgba(0,0,0,.6)'>
                            </Icon>
                                <Text style={{ fontSize: 18, fontWeight: '100', color: 'rgba(0,0,0,.6)', textAlign: 'center', justifyContent: 'center', alignItems: 'center', marginTop: 30 }} >
                                Añade el nombre de la empresa que quieres agregar a la lista.</Text>
                                    <Item inlineLabel style={{ marginTop: 30, borderBottomWidth: 0 }}>
                                        <Label style={{ color: 'rgba(0,0,0,.6)', fontSize: 18 }}>Empresa</Label>
                                        <Input style={{height: 40, borderColor: 'rgba(0,0,0,.4)', borderWidth: 1, color: 'rgba(0,0,0,.6)'}}
                                            returnKeyType="done"
                                            onChangeText={(sector)=>this.setState({sectorPrompt: sector})}
                                        />
                                    </Item>
                    </Col>
                    </Row>
                </Grid>       
            </View>
            <Divider style={{ backgroundColor: '#ddd', width: window.width - 20 }} />
            <View style={styles.modalContent}>
                <Grid style={{ alignItems: 'center', justifyContent: 'center', alignContent: 'center' }}>
                    <Col style={ styles.columnas }>
                            <TouchableOpacity
                             onPress={()=> {this._hideModal(); 
                                    this.setState({sectorPrompt: null})}}>
                            <Text style={{ color: 'rgba(0,0,0,.7)', textAlign: 'center', fontSize: 20 }}>Cancelar</Text>
                        </TouchableOpacity>
                    </Col>
                    <Col style={ styles.columnas }>
                            <TouchableOpacity
                             onPress={()=>  this.returnFromPrompt(this.state.sectorPrompt) }>
                            <Text style={{ color: 'rgba(0,0,0,.7)', textAlign: 'center', fontSize: 20 }}>Aceptar</Text>
                        </TouchableOpacity>
                    </Col>
                </Grid>
            </View>
        </Modal>

        </Content>

       

            {this.managePlatform()}

 
          


       
             
      </Container>
    );

    
}

showPrompt(){
    AlertIOS.prompt(
  'Crear nueva empresa',
  'Introduce el nombre de la nueva empresa',[
    {text: 'Cancelar'},
    {text: 'Aceptar', onPress: (text)=> this.returnFromPrompt(text) }
  ]
);
}

async returnEmpresa(){
   
  list = this.state.empresas;
  arr = this.state.empresasArr;

  for(let i in list){
    if(list[i]){
      local = arr[i].empresa;
      break;
    }
  }
    await AsyncStorage.setItem('id_token', "001");
    this.props.navigation.state.params.onGoBack(local);
    this.props.navigation.goBack();
 
}
async returnFromPrompt(text){
  
  local = text;
  await AsyncStorage.setItem('id_token', "001");
    this.props.navigation.state.params.onGoBack(local);
    this.props.navigation.goBack();
}
}




var styles = StyleSheet.create({
    headerText: {
        fontSize: 22,
        fontWeight: '100',
        color: '#fff',
        textAlign: 'center',
        marginTop: 8
    },
    headerJob: {
        fontSize: 16,
        fontWeight: '100',
        color: '#fff',
        textAlign: 'center'
    },
    headerTile: {
        fontSize: 16,
        fontWeight: '100',
        color: 'rgba(0,0,0,.6)',
        marginTop: 15,
        textAlign: 'center'
    },
    columnas: {
        borderWidth: 1, 
        borderColor: 'rgba(0,0,0,.1)',
        backgroundColor: 'rgba(255,255,255,1)',  
        justifyContent: 'center', 
        alignItems: 'center',
        height: 120
    },
    profile: {
        justifyContent: 'center', 
        alignItems: 'center', 
        marginTop: 22
    },
    actionButtonIcon: {
        height: 22,
        color: 'white',
    },
    container: {
        flex: 1,
        padding: 10
    },
    backgroundImage: {
        width: null,
        height: 300,
    },
    overlay: {
        position: 'absolute',
        top: 0,
        right: 0,
        bottom: 0,
        left: 0,
        backgroundColor: 'red',
        opacity: 0.3
    },
    modal: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    modal4: {
        height: 300
    },
    text: {
        color: "black",
        fontSize: 22
    },
    btn: {
        margin: 10,
        backgroundColor: "#3B5998",
        color: "white",
        padding: 10
    },

    btnModal: {
        position: "absolute",
        top: 0,
        right: 0,
        width: 50,
        height: 50,
        backgroundColor: "transparent"
    },
    button: {
        backgroundColor: 'lightblue',
        padding: 12,
        margin: 16,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        borderColor: 'rgba(0, 0, 0, 0.1)',
    },
    modalHeaderProspecto: {
        backgroundColor: '#fcfcfc',
        padding: 10,
        borderColor: 'rgba(0, 0, 0, 0.1)',
        height: 350
    },
    modalHeaderVinculo: {
        backgroundColor: 'rgba(189,9,38,1)',
        padding: 10,
        borderColor: 'rgba(0, 0, 0, 0.1)',
        height: 350
    },
    modalAvatar: {
        alignItems: 'center',
        flex: 1
    },
    modalContent: {
        backgroundColor: 'rgba(255,255,255,1)',
        height: 70,
        borderColor: 'rgba(0, 0, 0, 0.1)',
        alignItems: 'center', 
        justifyContent: 'center'
    },
    bottomModal: {
        justifyContent: 'flex-end',
        margin: 0,
    }
});




AppRegistry.registerComponent("selectEmpresa", ()=>selectEmpresa);