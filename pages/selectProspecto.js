import React, { Component, } from 'react';
import { AppRegistry, AlertIOS, Platform, TouchableOpacity, AsyncStorage, Animated, Dimensions, View } from 'react-native';
import { Container, Content, ListItem, Text, Radio, Right, Button, Icon } from 'native-base';
import Animation from 'lottie-react-native';
import TimerMixin from 'react-timer-mixin';
import { ImagePicker, Components, BlurView } from 'expo';
import ActionButton from 'react-native-action-button';

import Display from 'react-native-display';
let window = Dimensions.get('window')

export default class selectProspecto extends React.Component {
constructor(props){
    super(props);
    this.navigation = this.props.navigation;
    this.promptResult;
    this.params = this.props.navigation.state;
    this.prospectoData = [];
      this.timeouts = [];
    this.state = {
   
    timePassed: false,
      prospectos: this.prospectoData,
      prospectosV: null,
       dataLoaded: false,
              isActionButtonVisible: true,
      nombreState: null,
      apellidoState: null,
      maternoState: null,
      idProspecto: null,

    };
}

_listViewOffset = 0


_onScroll = (event) => {
  // Simple fade-in / fade-out animation
  const CustomLayoutLinear = {
    duration: 100,
    create: { type: LayoutAnimation.Types.linear, property: LayoutAnimation.Properties.opacity },
    update: { type: LayoutAnimation.Types.linear, property: LayoutAnimation.Properties.opacity },
    delete: { type: LayoutAnimation.Types.linear, property: LayoutAnimation.Properties.opacity }
  }
  // Check if the user is scrolling up or down by confronting the new scroll position with your own one
  const currentOffset = event.nativeEvent.contentOffset.y
  const direction = (currentOffset > 0 && currentOffset > this._listViewOffset)
    ? 'down'
    : 'up'
  // If the user is scrolling down (and the action-button is still visible) hide it
  const isActionButtonVisible = direction === 'up'
  if (isActionButtonVisible !== this.state.isActionButtonVisible) {
    LayoutAnimation.configureNext(CustomLayoutLinear)
    this.setState({ isActionButtonVisible })
  }
  // Update your scroll position
  this._listViewOffset = currentOffset

}


getRadios(){
  for(let i in this.state.prospectosV){
    this.prospectoData.push(false);
  }
console.log(this.prospectoData);
  this.setState({prospectos: this.prospectoData});
}

 getData(index) {
   console.log(index.i);
   console.log(this.prospectoData);
      for(let i in this.prospectoData){
        console.log(i);
        if(i == index.i){
          this.prospectoData[i] = true;
        }else{
          this.prospectoData[i] = false;
        }
      }
      this.setState({prospectos: this.prospectoData});
      console.log(this.state.prospectos);
  }

static navigationOptions  = ({navigation}) => {
    const { params = {} } = navigation.state
    const { nav = {} } = navigation;
    return{
    title: 'Selecciona un Prospecto',
   
    headerTintColor: '#FFF',
    headerStyle: {
        backgroundColor: '#41413f'
    }, 
}
}

componentDidMount(){
        fetch('http://innovatio.mx/reactAcem/getProspectosVinculos.php',{
            method: 'POST',
            header: {
                 'Accept': 'application/json',
               'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                idUsuario: this.navigation.state.params.idUsuario,
            })
        }).then((response)=>response.json())
        .then(( responseJSON ) => { 
              if(responseJSON.result == 'vacio'){
              }
              else{
                        this.setState({prospectosV:  responseJSON}, console.log('///////Inside Fetch Select idUsuario' + this.navigation.state.params.idUsuario));
                        this.setState({dataLoaded: true});
                        console.log(responseJSON);
                        this.getRadios()
              }
         })
        .catch((error) => {
        console.error(error);
      });
this.props.navigation.setParams({ handleSave: this.returnProspecto.bind(this)});
this.animation.play();
 this.timeouts.push(setTimeout(() => {
    this.setState({ timePassed: true });
  }, 3000));
}


clearTimeouts(){
  this.timeouts.forEach(clearTimeout);
}
  
componentWillUnmount(){
  this.clearTimeouts();
}


render(){
      const {setParams} = this.props.navigation;
      if(!this.state.timePassed || !this.state.dataLoaded){
         return (
                  <Display  enter={'fadeIn'} exit={'fadeOut'}  enable={!this.state.timePassed} style={{position: 'absolute', zIndex: 100000, width: window.width, height: window.height}}>
           <BlurView tint="dark" intensity={100} style={{position: 'absolute', zIndex: 100, width: window.width, height: window.height-50}} >
            <View style={{ flex: 1, justifyContent: 'center', alignContent: 'center' }}>
      <View
        style={{
           justifyContent: 'center', alignContent: 'center',width: window.width , marginLeft: window.width/2-100
        }}
      >
        <Animation
          style={{
            width:200,
            height:200 ,
          }}
          source={require('../assets/data12.json')}
          loop
         
          ref={(c)=> this.animation= c}
        />
      </View>
                     <Text style={{textAlign: 'center', color:'white', width: window.width}}>Cargando...</Text>
 

    </View>  
      </BlurView>
          </Display>)
       }

    return(
        <Container>
        <Content>
          {(this.state.prospectosV).map((l,i) => (
          <ListItem key={i + "Item"}
              onPress={()=> {this.getData({i}); 
              this.state.nombreState = l.nombre; 
              this.state.apellidoState = l.apellido_pat; 
              this.state.maternoState = l.apellido_mat; 
              this.state.idProspecto = l.id;
              console.log(this.state.nombreState);
              console.log(this.state.apellidoState);
              console.log(this.state.maternoState);
              console.log(this.state.idProspecto);
            }}>
            <Text key={i + "Text"}>{l.nombre} {l.apellido_pat} {l.apellido_mat}</Text>
            <Right key={i + "Right"}>
              <Radio key={i + "Radio"} onPress={()=>this.getData({i})} selected={this.state.prospectos[i]}  />
            </Right>
          </ListItem>
          ))}
        </Content>
   <Button block onPress={()=>  this.returnProspecto(this.state.idProspecto, this.state.nombreState, this.state.apellidoState, this.state.maternoState) } rounded style={{marginBottom: 20, marginLeft: 20, marginRight: 20 , backgroundColor: '#41413f'}}>
            <Text style={{ color: '#fff' }}>Aceptar</Text>
    </Button>
      </Container>
    );

    
}

async returnProspecto(id, nombre, apellido_pat, apellido_mat){

  console.log('Inside returnEmpresa' + id )
  console.log('Inside returnEmpresa' + nombre )
  console.log('Inside returnEmpresa' + apellido_pat )
  console.log('Inside returnEmpresa' + apellido_mat )

    await AsyncStorage.setItem('id_token', "001");
    this.props.navigation.state.params.onGoBack(id, nombre, apellido_pat, apellido_mat);
    this.props.navigation.goBack();
  console.log('Inside goback Vinculo' + id )
  console.log('Inside goback Vinculo' + nombre )
  console.log('Inside goback Vinculo' + apellido_pat )
  console.log('Inside goback Vinculo' + apellido_mat )

}
}



AppRegistry.registerComponent("selectProspecto", ()=>selectProspecto);