import React, { Component } from 'react';
import { Container, Content, Button, Item } from 'native-base';
import { Avatar, Tile, Header, Card, Icon, Divider, List, ListItem  } from 'react-native-elements'
import { Text, View, StyleSheet, TouchableOpacity, Image, AsyncStorage ,  Animated , AppRegistry, Dimensions, StatusBar, Alert } from 'react-native'
import { Col, Row, Grid } from 'react-native-easy-grid';
import { ImagePicker, Components, BlurView } from 'expo';
import Animation from 'lottie-react-native';
import TimerMixin from 'react-timer-mixin';

import Display from 'react-native-display';
let window = Dimensions.get('window')


export default class HomeScreen extends Component {

    constructor(props){
    super(props);
    this.navigation = this.props.navigation;
    this.params = this.props.navigation.state;
    this.timeouts = [];
    console.log(this.params.user);
     this.state = {
      progress: new Animated.Value(0),
      timePassed: false,
      nombre: this.navigation.state.params.nombre,
      apellido_pat: this.navigation.state.params.apellido_pat,
      apellido_mat: this.navigation.state.params.apellido_mat,
      empresa: this.navigation.state.params.empresa,
      puesto: this.navigation.state.params.puesto,
      id: this.navigation.state.params.id,
      url: this.navigation.state.params.url
    };

    
}


static navigationOptions = {
    title: 'Inicio',
    headerTintColor: '#FFF',
    header: null,
     headerStyle: {
        backgroundColor: '#41413f'
    },
  };
state = {
    image: null,
  };
   _pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      allowsEditing: true,
      aspect: [4, 3],
    });
     console.log(result);

    if (!result.cancelled) {
      this.setState({ image: result.uri });
    }
  };

 componentDidMount() {
StatusBar.setBarStyle('light-content', true);
        StatusBar.setBackgroundColor('black', true);
this.animation.play();
 this.timeouts.push(setTimeout(() => {
    this.setState({ timePassed: true, url: this.navigation.state.params.url });
  } , 3250));
}

clearTimeouts(){
  this.timeouts.forEach(clearTimeout);
}
  
componentWillUnmount(){
  this.clearTimeouts();
}


UploadUserImage(base64){
  fetch('http://innovatio.mx/reactAcem/imageUsers.php', {
    method: 'POST',
    header: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      id: this.navigation.state.params.id,
      baseImage: base64
      
    })
  }).then((response) => response.json()).then((responseJSON) => {
    this.setState({url: responseJSON.url});
    this.forceUpdate();
  }).catch((error) => {
    console.error(error);
  });
}

getuserData(){
  fetch('http://innovatio.mx/reactAcem/getUser.php', {
    method: 'POST',
    header: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      correo: this.navigation.state.params.correo,
      pass: this.navigation.state.params.pass
    })
  }).then((response) => response.json()).then((response) => {
      console.log(response);
      this.setState({nombre: response.nombre, apellido_mat: response.apellido_mat, apellido_pat: response.apellido_pat, puesto: response.puesto, empresa: response.empresa})
      this.forceUpdate();
  }).catch((error) => {
    console.error(error);
  })
}

async editProfile(navigation, params) {
  const DEMO_TOKEN = await AsyncStorage.getItem('id_token2');
  this.navigation.navigate('EditPerfilScreen', {
    onGoBack: () => 
      this.getuserData(),
      id: this.state.id,
      nombre: this.state.nombre, 
      apellido_pat: this.state.apellido_pat, 
      apellido_mat: this.state.apellido_mat, 
      puesto: this.state.puesto, 
      empresa: this.state.empresa , 
      correo: this.state.correo,
       url: this.state.image
    
  });
}


async getPhoto(){
  result = await ImagePicker.launchCameraAsync(
    {
      quality: 0.5,
      aspect: [1, 1],
      allowsEditing: true,
      base64: true
    }
  );
  console.log(result);
  if(!result.cancelled){
  this.UploadUserImage(result.base64);
  }


}

getAvatar(){
  if(this.navigation.state.params.url == null){
    return (<TouchableOpacity onPress={()=>this.getPhoto()} >
    <Avatar
      xlarge
      rounded
      title={(this.state.nombre).charAt(0) + (this.state.apellido_pat).charAt(0) }
      
      activeOpacity={0.7}
    />
    </TouchableOpacity>);
  }
  else{
    return (
    <TouchableOpacity onPress={()=>this.getPhoto()} >
    <Avatar xlarge  rounded  source={{uri: this.state.url}} />
      </TouchableOpacity>);
  }
}


logOut(){
Alert.alert(
  'Cerrar Sesión',
  '¿Deseas cerrar tu sesión?',
  [
    {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
    {text: 'OK', onPress: () => this.props.navigation.goBack()},
  ],
  { cancelable: false }
)
}

  render() {
     let that = this;

    return (
        <View style={{ flex: 1 }}>
           

          <Grid>
              <Row size={6} style={{ backgroundColor: '#41413f' }}>
                <Col style={styles.profile}>
                  <Row size={.5} style={{ backgroundColor: '#41413f' }}>
                  <Col style={styles.settings}> 
                  <TouchableOpacity onPress={()=>this.editProfile()}>
                      <Icon
                                name='settings'
                                type='simple-line-icon'
                                color='#fff'
                                size={25}
                            />
                    </TouchableOpacity>
                  </Col>
                  <Col>
                  </Col>
                  <Col style={styles.logout}>
                    <TouchableOpacity onPress={()=>this.logOut()}>
                      <Icon
                                name='logout'
                                type='simple-line-icon'
                                color='#fff'
                                size={25}
                            />
                    </TouchableOpacity>
                  </Col>
                  </Row>
                  {this.getAvatar()}
                  <Text style={styles.headerText}>{this.state.nombre} {this.state.apellido_pat} {this.state.apellido_mat}</Text>
                  <Text style={styles.headerJob}>{this.state.empresa} / {this.state.puesto}</Text>
                </Col>
              </Row>
              <Row size={4} style={{ backgroundColor: '#fff' }}>
                <Col style={styles.columnas}>
                 <TouchableOpacity onPress={()=>this.navigation.navigate('Prospectos', {id: this.state.id})}>
                      <Icon
                            name='event'
                            type='simple-line-icon'
                            color='#41413f'
                            size={50}
                      /> 
                      <Text style={styles.headerTile}>Prospectos</Text>
                       
                   </TouchableOpacity>
                  </Col>
                  <Col style={styles.columnas}>
                 <TouchableOpacity onPress={()=>this.navigation.navigate('Minutas', {id: this.state.id})}>
                      <Icon
                            name='notebook'
                            type='simple-line-icon'
                            color='#41413f'
                            size={50}
                      />
                      <Text style={styles.headerTile}>Minutas</Text>
                   </TouchableOpacity>
                  </Col>
              </Row>
              <Row size={4} style={{ backgroundColor: '#fff' }}>
                <Col style={styles.columnas}>
                 <TouchableOpacity  onPress={()=>this.navigation.navigate('Sector',  {idUsuario: this.navigation.state.params.id})}>
                      <Icon
                            name='organization'
                            type='simple-line-icon'
                            color='#41413f'
                            size={50}
                      />
                      <Text style={styles.headerTile}>Alianzas</Text>
                   </TouchableOpacity>
                </Col>
                <Col style={styles.columnas}>
                 <TouchableOpacity onPress={()=>this.navigation.navigate('Tabs', {idUsuario: this.navigation.state.params.id})}>
                      <Icon
                            name='user-follow'
                            type='simple-line-icon'
                            color='#41413f'
                            size={50} 
                      />
                      <Text style={styles.headerTile}>Vínculos</Text>
                   </TouchableOpacity>
                </Col>
              </Row>
          </Grid>
          <Display  enter={'fadeIn'} exit={'fadeOut'}  enable={!this.state.timePassed} style={{position: 'absolute', zIndex: 100, width: window.width, height: window.height}}>
           <BlurView tint="dark" intensity={100} style={{position: 'absolute', zIndex: 100, width: window.width, height: window.height}} >
            <View style={{ flex: 1, justifyContent: 'center', alignContent: 'center' }}>
      <View
        style={{
           justifyContent: 'center', alignContent: 'center',width: window.width , marginLeft: window.width/2-100
        }}
      >
        <Animation
          style={{
            width:200,
            height:200 ,
          }}
          source={require('../assets/data12.json')}
          loop
         
          ref={(c)=> this.animation= c}
        />
      </View>
                     <Text style={{textAlign: 'center', color:'white', width: window.width}}>Cargando...</Text>
 

    </View>  
      </BlurView>
          </Display>

        </View>
    );
  }
}
var styles = StyleSheet.create({
    headerText: {
        fontSize: 22,
        fontWeight: '100',
        color: '#fff',
        textAlign: 'center',
        marginTop: 10,
    },
    headerJob: {
        fontSize: 16,
        fontWeight: '100',
        color: '#fff',
        textAlign: 'center',
                marginBottom: 10

    },
    headerTile: {
        fontSize: 20,
        fontWeight: '100',
        color: 'rgba(0,0,0,.6)',
        marginTop: 40,
        textAlign: 'center'
    },
    columnas: {
        borderWidth: 1, 
        borderColor: 'rgba(0,0,0,.1)',
        backgroundColor: '#fff',  
        justifyContent: 'center', 
        alignItems: 'center'
    },
    profile: {
        justifyContent: 'center', 
        alignItems: 'center', 
        marginTop: 5
    },
    logout: {
        justifyContent: 'flex-end', 
        alignItems: 'center',
        marginLeft: 100
    },
    settings: {
      justifyContent: 'flex-end',
      alignItems: 'center'
    }
});