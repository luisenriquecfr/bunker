import React from 'react';
import {
  AppRegistry,
  Text,
  ScrollView, View, Dimensions, Alert, AsyncStorage
} from 'react-native';
import { Container, Content, Form, Item, Input, Label, Button, Separator } from 'native-base';
import { ImagePicker, Components, BlurView } from 'expo';
import Animation from 'lottie-react-native';
import TimerMixin from 'react-timer-mixin';

import Display from 'react-native-display';
let window = Dimensions.get('window')
import { StackNavigator } from 'react-navigation';

 export default class EditPerfilScreen extends React.Component {
constructor(props){
  super(props);
  this.timeouts = [];
  this.navigation = this.props.navigation;
  this.state = {
    timePassed: false,
    nombre: this.navigation.state.params.nombre,
    apellido_pat: this.navigation.state.params.apellido_pat,
    apellido_mat: this.navigation.state.params.apellido_mat,
    empresa: this.navigation.state.params.empresa,
    puesto: this.navigation.state.params.puesto,
   
  }
}

   focusNext(nextInput){
      nextInput._root.focus();
  }

getUserData(){

}

animationPlay(){
  this.setState({timePassed: true});
  
  this.animation.play();
   this.timeouts.push(setTimeout(() => {
    this.setState({ timePassed: false });
    this.navigation.goBack();
  }, 3250));
}

  static navigationOptions = {
    title: 'Editar Perfil',
    headerTintColor: '#FFF',

     headerStyle: {
        backgroundColor: '#41413F'
    },
  };

updateData(nombre, apellido_mat, apellido_pat, empresa, puesto){
  fetch('http://innovatio.mx/reactAcem//updateProfile.php',{
    method: 'POST',
    header: {
         'Accept': 'application/json',
       'Content-Type': 'application/json',
    },
    body: JSON.stringify({
        id: this.navigation.state.params.id ,
        nombre: nombre,
        apellido_pat: apellido_pat,
        apellido_mat: apellido_mat,
        empresa: empresa,
        puesto: puesto
    })
}).then((response)=>response.json())
.then((responseJSON)=> {this.animationPlay()} )
.catch((error) => {
console.error(error);
});
}



   createUser(nombre, apellido_pat, apellido_mat, puesto, empresa, correo){
     this.updateData(nombre, apellido_mat, apellido_pat, empresa, puesto);
     this.returnPerfil();
    }
      manageCreate(response){
          if(response.result == 'exito'){
      Alert.alert(
        'Éxito',
        'Cuenta creada satisfactoriamente',
        [
          {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
          {text: 'OK', onPress: () => console.log('OK Pressed')},
        ],
        { cancelable: false }
      )
                this.animationPlay();
          }
          else{
              Alert.alert(
                  'Error',
                  'Ocurrio un error, intenta más tarde',
                  [
                      {text: 'Aceptar'}
                  ]
              )
          }
      }



  render() {
    return (
    
   <Container>
        <Content >
          <Form style={{backgroundColor: 'white'}}>
                <Separator bordered>
                <Text>Datos de Personales</Text>
            </Separator>
            <Item inlineLabel>
              <Label>Nombre</Label>
              <Input 
              defaultValue = {this.navigation.state.params.nombre}
              returnKeyType="next"
              onSubmitEditing={()=> this.focusNext(this._pat)}
              onChangeText={(name)=> this.setState({nombre: name})}
                            keyboardAppearance="dark"

              />
              
            </Item>
            <Item inlineLabel >
              <Label>Apellido Paterno</Label>
              <Input 
              ref={(i) => this._pat = i}
              returnKeyType="next"
              defaultValue = {this.navigation.state.params.apellido_pat}

              onSubmitEditing={()=> this.focusNext(this._mat)}
              onChangeText={(pat)=> this.setState({apellido_pat: pat})}
              keyboardAppearance="dark"

              />
            </Item>
            <Item inlineLabel>
              <Label>Apellido Materno</Label>
              <Input
               ref={(i) => this._mat = i}
               defaultValue = {this.navigation.state.params.apellido_mat}

               onSubmitEditing={()=> this.focusNext(this._emp)}
              onChangeText={(mat)=> this.setState({apellido_mat: mat})}
              keyboardAppearance="dark"

              returnKeyType="next"/>
            </Item>
           
             <Separator bordered>
                <Text>Datos de cuenta</Text>
            </Separator>
            <Item inlineLabel>

              <Label>Empresa</Label>
              <Input
              ref={(i) => this._emp = i}
              defaultValue = {this.navigation.state.params.empresa}

               onSubmitEditing={()=> this.focusNext(this._pues)}
              onChangeText={(emp)=> this.setState({empresa: emp})}
              keyboardAppearance="dark"

              returnKeyType="next"/>
            </Item>
             <Item inlineLabel>
              <Label>Puesto</Label>
              <Input
              ref={(i) => this._pues = i}
              defaultValue = {this.navigation.state.params.puesto}

              onChangeText={(pues)=> this.setState({puesto: pues})}
              onSubmitEditing={()=> this.focusNext(this._mail)}
                            keyboardAppearance="dark"

              returnKeyType="next"/>
            </Item>
            
            
          </Form>
          
           <Button onPress={()=>{this.createUser(this.state.nombre, this.state.apellido_pat, this.state.apellido_mat, this.state.puesto, this.state.empresa)}} block rounded style={{marginTop: 15, marginLeft: 20, marginRight: 20 , backgroundColor: '#41413F'}}>
            <Text style={{color: 'white'}}>Editar Perfil</Text>
          </Button> 

        </Content>
<Display keepAlive={true} enter={'fadeIn'} exit={'fadeOut'}  enable={this.state.timePassed} style={{position: 'absolute', zIndex: 100, width: window.width, height: window.height}}>
           <BlurView tint="dark" intensity={100} style={{position: 'absolute', zIndex: 100, width: window.width, height: window.height}} >
            <View style={{ flex: 1, justifyContent: 'center', alignContent: 'center' }}>
      <View
        style={{
           justifyContent: 'center', alignContent: 'center',width: window.width , marginLeft: window.width/2-100
        }}
      >
        <Animation
          style={{
            width:200,
            height:200 ,
          }}
          source={require('../assets/data12.json')}
          loop
         
          ref={(c)=> this.animation = c}
        />
      </View>
                     <Text style={{textAlign: 'center', color:'white', width: window.width}}>Cargando...</Text>
 

    </View>  
      </BlurView>
          </Display>

      </Container>);
  }

  async returnPerfil(){
    await AsyncStorage.setItem('id_token2', "001");
  this.props.navigation.state.params.onGoBack();
  this.props.navigation.goBack();
}

}



const Registro = StackNavigator({
  Perfil: {screen: EditPerfilScreen}
});

AppRegistry.registerComponent('Perfil', () => Perfil);
