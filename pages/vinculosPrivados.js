import React, { Component } from 'react';
import { Container , Content, List, ListItem, Left, Body, Right, Thumbnail, CardItem, Button, Card } from 'native-base';
import { Avatar, Tile, Header, Icon, Divider  } from 'react-native-elements'
import {  Text, Dimensions, View, StyleSheet, TouchableOpacity, Image, AppRegistry, ScrollView, Platform, UIManager, LayoutAnimation } from 'react-native'
import { Col, Row, Grid } from 'react-native-easy-grid';
import { StackNavigator } from 'react-navigation';
import ActionButton from 'react-native-action-button';
import Modal from 'react-native-modal';
let window = Dimensions.get('window')

const vinculos = [{
    prospecto: 'PR',
    vinculos: 'YU'
  },{
    prospecto: 'TD',
    vinculos: 'OO'
  },{
    prospecto: 'MK',
    vinculos: 'LE'
  },{
    prospecto: 'VT',
    vinculos: 'BW'
  },{
    prospecto: 'AD',
    vinculos: 'PU'
  },{
    prospecto: 'CQ',
    vinculos: 'ZO'
  }
];
UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);
export default class VinculosPrivados extends Component {
constructor(props){
    super(props);
    this.navigation = this.props.navigation;
  this.state = {
    visibleModal: null,
    isActionButtonVisible: true 
  };
}
static navigationOptions = {
    title: 'Vinculos',
    headerTintColor: '#FFF',
     headerStyle: {
        backgroundColor: 'rgba(189,9,38,1)'
    },
  };
 _renderButtonProspecto = (text, onPress) => (
    <TouchableOpacity onPress={onPress}>
        <Avatar 
            large
            rounded
            title="LW"
            activeOpacity={0.7}
            overlayContainerStyle={{backgroundColor: '#3498db'}} 
        />
    </TouchableOpacity>  
  );
 _renderButtonVinculo = (text, onPress) => (
    <TouchableOpacity onPress={onPress}>
        <Avatar 
            large
            rounded
            title="TV"
            activeOpacity={0.7}
            overlayContainerStyle={{backgroundColor: 'rgba(189,9,38,1)'}}                       
        />
    </TouchableOpacity>
  );
 _renderButtonClose = (text, onPress) => (
    <TouchableOpacity onPress={onPress}>
        <Icon
            name='close'
            type='simple-line-icon'
            color='#fff'
            size={30}
            style={{ justifyContent: 'flex-end' }}
        />
    </TouchableOpacity>
  );
 _renderButtonRecordatorio = (text, onPress) => (
    <TouchableOpacity onPress={onPress}>
        <Icon
            name='calendar'
            type='simple-line-icon'
            color='#fff'
            size={30}
            style={{ justifyContent: 'flex-end' }}
        />
    </TouchableOpacity>
  );

_listViewOffset = 0


_onScroll = (event) => {
  // Simple fade-in / fade-out animation
  const CustomLayoutLinear = {
    duration: 300,
    create: { type: LayoutAnimation.Types.easeInEaseOut, property: LayoutAnimation.Properties.opacity },
    update: { type: LayoutAnimation.Types.easeInEaseOut, property: LayoutAnimation.Properties.opacity },
    delete: { type: LayoutAnimation.Types.easeInEaseOut, property: LayoutAnimation.Properties.opacity }
  }
  // Check if the user is scrolling up or down by confronting the new scroll position with your own one
  const currentOffset = event.nativeEvent.contentOffset.y
  const direction = (currentOffset > 0 && currentOffset > this._listViewOffset)
    ? 'down'
    : 'up'
  // If the user is scrolling down (and the action-button is still visible) hide it
  const isActionButtonVisible = direction === 'up'
  if (isActionButtonVisible !== this.state.isActionButtonVisible) {
    LayoutAnimation.configureNext(CustomLayoutLinear)
    this.setState({ isActionButtonVisible })
  }
  // Update your scroll position
  this._listViewOffset = currentOffset
}
getFab(){
    return (Platform.OS === 'android') ? <ActionButton onPress={()=>this.navigation.navigate('crearVinculo')} buttonColor="#bd1e52"></ActionButton> : null 
}
  render() {
    return (
      <Container style={{ flex: 1, padding: 10 }} >
        <Content onScroll={this._onScroll} >
            {
        vinculos.map((v, i) => (
          <Card key={i} >
            <CardItem  style={{width: window.width-20}}  >
                <Grid>
                    <Col size={5} style={{ backgroundColor: '#fff', height: 150, justifyContent: 'center', alignItems: 'center' }} >
                        {this._renderButtonProspecto('Prospecto modal', () => this.setState({ visibleModal: 1 }))}
                        <Text style={{ textAlign: 'center', fontWeight: '100', fontSize: 20, marginTop: 20 }} >Prospecto</Text>
                    </Col>
                    <Col size={2} style={{ backgroundColor: '#fff', height: 150, justifyContent: 'center', alignItems: 'center' }} >
                      <Icon
                            name='link'
                            type='simple-line-icon'
                            color='#2CC990'
                            size={40}
                      />
                    </Col>
                    <Col size={5} style={{ backgroundColor: '#fff', height: 150, justifyContent: 'center', alignItems: 'center' }} >
                        {this._renderButtonVinculo('Vinculo modal', () => this.setState({ visibleModal: 2 }))}
                        <Text style={{ textAlign: 'center', fontWeight: '100', fontSize: 20, marginTop: 20 }} >Vinculo</Text>
                    </Col>
                </Grid>
            </CardItem>
          </Card>
                ))
            } 
        <Modal isVisible={this.state.visibleModal === 1}>
            <View style={styles.modalHeaderProspecto}>
                <Grid>
                    <Col style={{ backgroundColor: '#3498db', height: 30 }} >
                        {this._renderButtonClose('Close', () => this.setState({ visibleModal: null }))}
                    </Col>
                    <Col style={{ backgroundColor: '#3498db', height: 30, alignItems: 'flex-end' }} >
                        {this._renderButtonRecordatorio('Recordtorio', () => this.setState({ visibleModal: null }))}
                    </Col>
                </Grid>
                            <Icon
                            name='user'
                            type='simple-line-icon'
                            color='#fff'
                            size={80}
                            style={{ alignItems: 'center', justifyContent: 'center' }}
                      />
                                <Text style={{ fontSize: 22, fontWeight: '100', color: '#fff', textAlign: 'center', justifyContent: 'center', alignItems: 'center', marginTop: 30 }} >
                                Lukas Wellington</Text>
            </View>
            <View style={styles.modalContent}>
                <Grid>
                    <Row style={{ height: 110, backgroundColor: '#fff' }} >
                        <Col style={styles.columnas}>
                            <TouchableOpacity>
                                <Icon
                                    name='phone'
                                    type='simple-line-icon'
                                    color='#3498db'
                                    size={42}
                                />
                                <Text style={styles.headerTile}>55386256</Text>
                            </TouchableOpacity>
                        </Col>
                        <Col style={styles.columnas}>
                            <TouchableOpacity>
                                <Icon
                                    name='envelope'
                                    type='simple-line-icon'
                                    color='#3498db'
                                    size={42}
                                />
                                <Text style={styles.headerTile}>lukas@gmail.com</Text>
                            </TouchableOpacity>
                        </Col>
                    </Row>
                    <Row style={{ height: 120, backgroundColor: '#fff' }} >
                        <Col style={styles.columnas}>
                            <TouchableOpacity>
                                <Icon
                                    name='organization'
                                    type='simple-line-icon'
                                    color='#3498db'
                                    size={42}
                                />
                                <Text style={styles.headerTile}>Kaitensoft</Text>
                            </TouchableOpacity>
                        </Col>
                        <Col style={styles.columnas}>
                            <TouchableOpacity>
                                <Icon
                                    name='briefcase'
                                    type='simple-line-icon'
                                    color='#3498db'
                                    size={42}
                                />
                                <Text style={styles.headerTile}>Director Comercial</Text>
                            </TouchableOpacity>
                        </Col>
                    </Row>
                </Grid>
            </View>
        </Modal>
        <Modal isVisible={this.state.visibleModal === 2}>
            <View style={styles.modalHeaderVinculo}>
                <Grid>
                    <Col style={{ backgroundColor: 'rgba(189,9,38,1)', height: 30 }} >
                        {this._renderButtonClose('Close', () => this.setState({ visibleModal: null }))}
                    </Col>
                    <Col style={{ backgroundColor: 'rgba(189,9,38,1)', height: 30, alignItems: 'flex-end' }} >
                        {this._renderButtonRecordatorio('Recordtorio', () => this.setState({ visibleModal: null }))}
                    </Col>
                </Grid>
                            <Icon
                            name='user'
                            type='simple-line-icon'
                            color='#fff'
                            size={80}
                            style={{ alignItems: 'center', justifyContent: 'center' }}
                      />
                                <Text style={{ fontSize: 22, fontWeight: '100', color: '#fff', textAlign: 'center', justifyContent: 'center', alignItems: 'center', marginTop: 30 }} >
                                Tobias Vergara</Text>
            </View>
            <View style={styles.modalContent}>
                <Grid>
                    <Row style={{ height: 110, backgroundColor: '#fff' }} >
                        <Col style={styles.columnas}>
                            <TouchableOpacity>
                                <Icon
                                    name='phone'
                                    type='simple-line-icon'
                                    color='rgba(189,9,38,1)'
                                    size={42}
                                />
                                <Text style={styles.headerTile}>5566227788</Text>
                            </TouchableOpacity>
                        </Col>
                        <Col style={styles.columnas}>
                            <TouchableOpacity>
                                <Icon
                                    name='envelope'
                                    type='simple-line-icon'
                                    color='rgba(189,9,38,1)'
                                    size={42}
                                />
                                <Text style={styles.headerTile}>tvergara@gmail.com</Text>
                            </TouchableOpacity>
                        </Col>
                    </Row>
                    <Row style={{ height: 120, backgroundColor: '#fff' }} >
                        <Col style={styles.columnas}>
                            <TouchableOpacity>
                                <Icon
                                    name='organization'
                                    type='simple-line-icon'
                                    color='rgba(189,9,38,1)'
                                    size={42}
                                />
                                <Text style={styles.headerTile}>Innovatio</Text>
                            </TouchableOpacity>
                        </Col>
                        <Col style={styles.columnas}>
                            <TouchableOpacity>
                                <Icon
                                    name='briefcase'
                                    type='simple-line-icon'
                                    color='rgba(189,9,38,1)'
                                    size={42}
                                />
                                <Text style={styles.headerTile}>Director de Innovación</Text>
                            </TouchableOpacity>
                        </Col>
                    </Row>
                </Grid>
            </View>
        </Modal>
      </Content>
      {this.state.isActionButtonVisible ? this.getFab() : null} 
    </Container>
    );
  }
}
VinculosPrivados.navigationOptions = props =>{
 const { navigation } = props;
  const { state, setParams } = navigation;
  const { params } = state;

return{
    title: 'Vinculos',
    headerTintColor: '#FFF',
    headerRight:  (Platform.OS === 'ios') ? <TouchableOpacity onPress={()=>navigation.navigate('crearVinculo')} ><Icon type='ionicon' name='ios-add'  color='white' size={27} style={{ marginRight: 20}} /></TouchableOpacity> : null  
,
     headerStyle: {
        backgroundColor: 'rgba(189,9,38,2)'
    },
}
}
var styles = StyleSheet.create({
    headerText: {
        fontSize: 22,
        fontWeight: '100',
        color: '#fff',
        textAlign: 'center',
        marginTop: 8
    },
    headerJob: {
        fontSize: 16,
        fontWeight: '100',
        color: '#fff',
        textAlign: 'center'
    },
    headerTile: {
        fontSize: 16,
        fontWeight: '100',
        color: 'rgba(0,0,0,.6)',
        marginTop: 15,
        textAlign: 'center'
    },
    columnas: {
        borderWidth: 1, 
        borderColor: 'rgba(0,0,0,.1)',
        backgroundColor: '#fff',  
        justifyContent: 'center', 
        alignItems: 'center',
        height: 120
    },
    profile: {
        justifyContent: 'center', 
        alignItems: 'center', 
        marginTop: 22
    },
    actionButtonIcon: {
        height: 22,
        color: 'white',
    },
    container: {
        flex: 1,
        padding: 10
    },
    backgroundImage: {
        width: null,
        height: 300,
    },
    overlay: {
        position: 'absolute',
        top: 0,
        right: 0,
        bottom: 0,
        left: 0,
        backgroundColor: 'red',
        opacity: 0.3
    },
    modal: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    modal4: {
        height: 300
    },
    text: {
        color: "black",
        fontSize: 22
    },
    btn: {
        margin: 10,
        backgroundColor: "#3B5998",
        color: "white",
        padding: 10
    },

    btnModal: {
        position: "absolute",
        top: 0,
        right: 0,
        width: 50,
        height: 50,
        backgroundColor: "transparent"
    },
    button: {
        backgroundColor: 'lightblue',
        padding: 12,
        margin: 16,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        borderColor: 'rgba(0, 0, 0, 0.1)',
    },
    modalHeaderProspecto: {
        backgroundColor: '#3498db',
        padding: 10,
        borderColor: 'rgba(0, 0, 0, 0.1)',
        height: 200
    },
    modalHeaderVinculo: {
        backgroundColor: 'rgba(189,9,38,1)',
        padding: 10,
        borderColor: 'rgba(0, 0, 0, 0.1)',
        height: 200
    },
    modalAvatar: {
        alignItems: 'center',
        flex: 1
    },
    modalContent: {
        backgroundColor: '#fff',
        height: 220,
        borderColor: 'rgba(0, 0, 0, 0.1)'
    },
    bottomModal: {
        justifyContent: 'flex-end',
        margin: 0,
    }
});
AppRegistry.registerComponent("VinculosPrivados", ()=> VinculosPrivados);