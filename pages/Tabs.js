import React, { Component } from 'react';
import { Container, Content, Button, List, ListItem, Left, Body, Right, Thumbnail, Card, CardItem, Tab, Tabs, Input, Form, Item, Label, Icon } from 'native-base';
import { Avatar, Tile, Header, Divider  } from 'react-native-elements'
import { Text, View, StyleSheet, TouchableOpacity, Platform , Image, AppRegistry,  Dimensions, UIManager, LayoutAnimation, AlertIOS, Alert } from 'react-native'
import { Col, Row, Grid } from 'react-native-easy-grid';
import { StackNavigator } from 'react-navigation';
import ActionButton from 'react-native-action-button';
import Publico from './Publico';
import Privado from './Privado';
import Vinculos from './Vinculos';
import VinculosPrivados from './vinculosPrivados'
import { LinearGradient } from 'expo';
import { ImagePicker, Components , BlurView} from 'expo';
import Animation from 'lottie-react-native';
import TimerMixin from 'react-timer-mixin';
import Display from 'react-native-display';
import Modal from 'react-native-modal';
import SearchBar from 'react-native-searchbar'
let window = Dimensions.get('window')
UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);
export default class TabsP extends Component {
constructor(props){
    super(props);
    this.navigation = this.props.navigation;
    this.params = this.props.navigation.state;
    this.timeouts = [];
    this.sectores = [];
    this.publicas = [];
    this.privadas = [];
     this.state = {
        navigation: this.props.navigation,
        timePassed: false,
        idUsuario: this.navigation.state.params.idUsuario,
        promptRes: null,
        publicas: [],
        privadas: [],
        publica: 'Publica',
        privada: 'Privada',
        isModalVisible: false,
        fetchCompleted: false, 
        currentIndex: 0,
        currentData: []
  }
}

fetchEmpresas(){
this.setState({timePassed: false, fetchCompleted: false})

    fetch('http://innovatio.mx/reactAcem/getPublicas.php',{
            method: 'POST',
            header: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                idUsuario: this.state.idUsuario
            })
        }).then(( response )=> response.json())
        .then((responseJSON) => { 
            this.setState({fetchCompleted: true, publicas: responseJSON})
            this.publicas = responseJSON
            console.log('//// Inside Get Data Publica' + this.publicas);
         });
    fetch('http://innovatio.mx/reactAcem/getPrivadas.php',{
            method: 'POST',
            header: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                idUsuario: this.state.idUsuario
            })
        }).then(( response )=> response.json())
        .then(( responseJSON )=> { 
            this.setState({timePassed: true})
              if(responseJSON.result == 'vacio'){

              }
              else{
                  this.setState({privadas: responseJSON});
                  this.privadas = responseJSON
              }
         })
        .catch((error) => {
        console.error(error);
      });
}
showSearch(){
this.searchBar.show()

}

componentDidMount() {
this.handleSearchData(0);
this.props.navigation.setParams({ handleSave: this.showPrompt.bind(this), handleSearch: this.showSearch.bind(this) });
    fetch('http://innovatio.mx/reactAcem/getPublicas.php',{
            method: 'POST',
            header: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                idUsuario: this.state.idUsuario
            })
        }).then(( response )=> response.json())
        .then((responseJSON) => { 
            this.setState({fetchCompleted: true, publicas: responseJSON})
            this.publicas = responseJSON
         });
    fetch('http://innovatio.mx/reactAcem/getPrivadas.php',{
            method: 'POST',
            header: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                idUsuario: this.state.idUsuario
            })
        }).then(( response )=> response.json())
        .then(( responseJSON )=> { 
                this.setState({timePassed: true});
              if(responseJSON.result == 'vacio'){

              }
              else{
                                    this.setState({privadas: responseJSON});

                  this.privadas = responseJSON
              }
         })
        .catch((error) => {
        console.error(error);
      });
console.log(new Date().toDateString());
this.animation.play();
 
}

clearTimeouts(){
  this.timeouts.forEach(clearTimeout);
}
  
componentWillUnmount(){
  this.clearTimeouts();
}

  _showModal = () => this.setState({ isModalVisible: true })

  _hideModal = () => this.setState({ isModalVisible: false })

 showPrompt(){
    if(Platform.OS === 'ios'){
    AlertIOS.prompt(
  'Crear nueva dependencía',
  'Introduce el nombre de la nueva dependencía',[
    {text: 'Cancelar', onPress: (text)=> {this.setState({promptRes: null})} },
    {text: 'Público', onPress: (text)=> {console.log(text); console.log(this.state.publica); this.setState({promptRes: text}); this.createDependencia(this.state.promptRes, this.state.publica) } },
    {text: 'Privado', onPress: (text)=> {console.log(text); console.log(this.state.privada); this.setState({promptRes: text}); this.createDependencia(this.state.promptRes, this.state.privada) } }
  ]
);
    }
    else{
        console.log('Inside Android (:');
        this._showModal();
    }
}

createDependencia( prompt, tipo ){
if ( prompt == null || prompt == '' ){
Alert.alert(
  'Error',
  'Revisa que hayas llenado correctamente el nombre de la Dependencia',
  [
    {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
    {text: 'OK', onPress: () => console.log('OK Pressed')},
  ],
  { cancelable: false }
)
}
else{
this._hideModal();
console.log('Inside Create Sector Nombre Dependencia' + prompt);
console.log('//// Create Sector Usuario' + this.state.idUsuario);
console.log('After Sctor Init' + tipo);
    fetch('http://innovatio.mx/reactAcem/createDependencia.php',{
        method: 'POST',
        header: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                idUsuario: this.state.idUsuario,
                tipo_dependencia: tipo,
                nombre_dependencia: prompt
            })
        }).then((response)=> response.json())
        .then(( responseJSON )=> { 
              if(responseJSON.result == 'vacio'){

              }
              else{
                Alert.alert(
                'Éxito',
                'La Dependencia se ha agregado exitosamente',
                [
                    {text: 'OK', onPress: () =>{ console.log('OK Pressed'); this.fetchEmpresas() ; this.forceUpdate()}},
                ],
                { cancelable: false }
                )
              }
              this.setState({promptRes: null})
         })
        .catch((error) => {
        console.error(error);
      });
}
}

static navigationOptions  = ({navigation}) => {
    const { params = {} } = navigation.state
    const { nav = {} } = navigation;
    return{
    title: 'Red de Vinculos',
    headerTintColor: '#FFF',
    headerRight:  (Platform.OS === 'ios' || Platform.OS === 'android' ) ? <View style={{flex: 1,  flexDirection: 'row', justifyContent: 'center',
        alignItems: 'center',}}><TouchableOpacity onPress={()=>{params.handleSearch()}}
 ><Icon style={{fontSize: 25,  color: '#fff',  marginRight: 20}} ios="ios-search-outline" android="md-search"></Icon></TouchableOpacity>
 <TouchableOpacity onPress={()=>{params.handleSave()}}
 ><Icon style={{fontSize: 30, color: '#fff',  marginRight: 20}} ios="ios-add" android="md-add"></Icon></TouchableOpacity>
 </View> : null,
     headerStyle: {
        backgroundColor: '#41413f',
        elevation: 0
    }, 
}
}


_listViewOffset = 0


_onScroll = (event) => {
  // Simple fade-in / fade-out animation
  const CustomLayoutLinear = {
    duration: 300,
    create: { type: LayoutAnimation.Types.easeInEaseOut, property: LayoutAnimation.Properties.opacity },
    update: { type: LayoutAnimation.Types.easeInEaseOut, property: LayoutAnimation.Properties.opacity },
    delete: { type: LayoutAnimation.Types.easeInEaseOut, property: LayoutAnimation.Properties.opacity }
  }
  // Check if the user is scrolling up or down by confronting the new scroll position with your own one
  const currentOffset = event.nativeEvent.contentOffset.y
  const direction = (currentOffset > 0 && currentOffset > this._listViewOffset)
    ? 'down'
    : 'up'
  // If the user is scrolling down (and the action-button is still visible) hide it
  const isActionButtonVisible = direction === 'up'
  if (isActionButtonVisible !== this.state.isActionButtonVisible) {
    LayoutAnimation.configureNext(CustomLayoutLinear)
    this.setState({ isActionButtonVisible })
  }
  // Update your scroll position
  this._listViewOffset = currentOffset
}
_handleResults(results) {
  this.publicas = results;
  this.privadas = results;
  this.forceUpdate();
}
_handleResults2(results){
    this.privadas = results;
    this.forceUpdate;
}
handleSearchBar(index){
    this.handleSearchData(index);
    this.setState({currentIndex: index});
    this.resetData();
    this.forceUpdate();
}

handleSearchData(index){
    if(index == 0){
        this.setState({currentData: this.publicas});
    }else{
        this.setState({ currentData: this.privadas });
    }
}

resetData(){
    this.privadas = this.state.privadas;
    this.publicas = this.state.publicas;
}

getFab(){
    return (Platform.OS === 'android') ? <ActionButton  buttonTextStyle={{ color: '#41413f' }} buttonColor="#fff" style={{ marginTop: 300 }}></ActionButton> : null 
}


  render() {
    return (
        <View style={{width: window.width, height: window.height}} >

            <SearchBar
          ref={(ref) => this.searchBar = ref}
           data={this.state.currentData}
  handleResults={this._handleResults.bind(this)}
  allDataOnEmptySearch={true}
        />
        <Tabs onChangeTab={({i, ref})=> this.handleSearchBar(i)} initialPage={0} tabStyle={{ backgroundColor: '#41413f' }}>
          <Tab heading="Públicas" textStyle={{ color: 'rgba(255,255,255,.8)' }} activeTextStyle={{ color: 'rgba(255,255,255,1)' }} tabStyle={{ backgroundColor: '#41413f' }} activeTabStyle={{ backgroundColor: '#41413f' }}>
            <Container>
                <Content>
                <List>
                    {
                        this.publicas.map((publicas, i) => (
                            <ListItem onPress={()=>this.navigation.navigate('Vinculos', { idRed: publicas.id, nombreRed: publicas.nombre_dependencia, idUsuario: this.state.idUsuario })} avatar>
                            <Left>
                                <Avatar
                                medium
                                rounded
                                key={i}
                                title={(publicas.nombre_dependencia).charAt(0)}
                                onPress={() => console.log("Works!")}
                                activeOpacity={0.7}
                                overlayContainerStyle={{backgroundColor: '#cfcabb'}}
                                />
                            </Left>
                            <Body>
                            
                                <Text>{publicas.nombre_dependencia}</Text>
                                <Text numberOfLines={1}  note></Text>
                                <Text numberOfLines={1}  note></Text>
                                <Text numberOfLines={1}  note></Text>
                                <Text numberOfLines={1}  note></Text>
                            </Body>
                            </ListItem>
                        ))
                    }
                </List>
                </Content>
            </Container> 
          </Tab>
          <Tab heading="Privadas" textStyle={{ color: 'rgba(255,255,255,.8)' }} activeTextStyle={{ color: 'rgba(255,255,255,1)' }} tabStyle={{ backgroundColor: '#41413f' }} activeTabStyle={{ backgroundColor: '#41413f' }}>
            <Container>
                <Content>
                <List>
                    {
                        this.privadas.map((privadas, i) => (
                            <ListItem onPress={()=>this.navigation.navigate('Vinculos', { idRed: privadas.id, nombreRed: privadas.nombre_dependencia, idUsuario: this.state.idUsuario })} avatar>
                            <Left>
                                <Avatar
                                medium
                                rounded
                                key={i}
                                title={(privadas.nombre_dependencia).charAt(0)}
                                onPress={() => console.log("Works!")}
                                activeOpacity={0.7}
                                overlayContainerStyle={{backgroundColor: '#953163'}}
                                />
                            </Left>
                            <Body>
                            
                                <Text>{privadas.nombre_dependencia}</Text>
                                <Text numberOfLines={1}  note></Text>
                                <Text numberOfLines={1}  note></Text>
                                <Text numberOfLines={1}  note></Text>
                                <Text numberOfLines={1}  note></Text>
                            </Body>
                            </ListItem>
                        ))
                    }
                </List>
                </Content>
            </Container> 
          </Tab>
        </Tabs>
           <Display keepAlive={true} enter={'fadeIn'} exit={'fadeOut'}  enable={!this.state.timePassed || !this.state.fetchCompleted} style={{position: 'absolute', zIndex: 100, width: window.width, height: window.height}}>
           <BlurView tint="dark" intensity={100} style={{position: 'absolute', zIndex: 100, width: window.width, height: window.height}} >
            <View style={{ flex: 1, justifyContent: 'center', alignContent: 'center' }}>
      <View
        style={{
           justifyContent: 'center', alignContent: 'center',width: window.width , marginLeft: window.width/2-100
        }}
      >
        <Animation
          style={{
            width:200,
            height:200 ,
          }}
          source={require('../assets/data12.json')}
          loop
         
          ref={(c)=> this.animation = c}
        />
      </View>
                     <Text style={{textAlign: 'center', color:'white', width: window.width}}>Cargando...</Text>
 

    </View>  
      </BlurView>
          </Display>
       <Modal isVisible={this.state.isModalVisible}>
            <View style={styles.modalHeaderProspecto}>
                <Grid>
                    <Row size={1}>
                    <Col style={{ backgroundColor: 'rgba(255,255,255,.0)', height: 30 }} >
                            <TouchableOpacity
                             onPress={()=> {this._hideModal(); 
                                    this.setState({promptRes: null})}}>
                            <Icon style={{fontSize: 35, color: 'rgba(0,0,0,.6)', justifyContent: 'flex-end'}} ios="ios-close" android="md-close" color='rgba(0,0,0,.6)'>
                            </Icon>
                        </TouchableOpacity>
                    </Col>
                    <Col style={{ backgroundColor: 'rgba(255,255,255,.0)', height: 30, alignItems: 'flex-end' }} >
                    </Col>
                    </Row>
                    <Row size={4}>
                    <Col style={{ alignItems: 'center', alignContent: 'center', justifyContent: 'center' }} >
                        <Icon style={{fontSize: 88, color: 'rgba(0,0,0,.6)', alignItems: 'center', justifyContent: 'center'}} ios="ios-briefcase" android="md-briefcase" color='rgba(0,0,0,.6)'>
                            </Icon>
                                <Text style={{ fontSize: 18, fontWeight: '100', color: 'rgba(0,0,0,.6)', textAlign: 'center', justifyContent: 'center', alignItems: 'center', marginTop: 30 }} >
                                Añade el nombre de la Dependencia que quieres agregar a la lista y selecciona entre Público y Privado.</Text>
                                    <Item inlineLabel style={{ marginTop: 30, borderBottomWidth: 0 }}>
                                        <Label style={{ color: 'rgba(0,0,0,.6)', fontSize: 18 }}>Dependencia</Label>
                                        <Input style={{height: 40, borderColor: 'rgba(0,0,0,.4)', borderWidth: 1, color: 'rgba(0,0,0,.6)'}}
                                            returnKeyType="done"
                                            onChangeText={(dependencia)=> this.setState({promptRes: dependencia})}
                                        />
                                    </Item>
                    </Col>
                    </Row>
                </Grid>       
            </View>
            <Divider style={{ backgroundColor: '#ddd', width: window.width - 20 }} />
            <View style={styles.modalContent}>
                <Grid style={{ alignItems: 'center', justifyContent: 'center', alignContent: 'center' }}>
                    <Col style={ styles.columnas }>
                            <TouchableOpacity
                             onPress={()=> {this._hideModal(); 
                                    this.setState({promptRes: null})}}>
                            <Text style={{ color: 'rgba(0,0,0,.7)', textAlign: 'center', fontSize: 20 }}>Cancelar</Text>
                        </TouchableOpacity>
                    </Col>
                    <Col style={ styles.columnas }>
                            <TouchableOpacity
                             onPress={()=>  {console.log(this.state.promptRes); console.log(this.state.publica); this.createDependencia(this.state.promptRes, this.state.publica)} }>
                            <Text style={{ color: 'rgba(0,0,0,.7)', textAlign: 'center', fontSize: 20 }}>Público</Text>
                        </TouchableOpacity>
                    </Col>
                    <Col style={ styles.columnas }>
                            <TouchableOpacity
                             onPress={()=>  {console.log(this.state.promptRes); console.log(this.state.privada); this.createDependencia(this.state.promptRes, this.state.privada)} }>
                            <Text style={{ color: 'rgba(0,0,0,.7)', textAlign: 'center', fontSize: 20 }}>Privado</Text>
                        </TouchableOpacity>
                    </Col>
                </Grid>
            </View>
        </Modal>
      </View>

    );
  }
}
var styles = StyleSheet.create({
    headerText: {
        fontSize: 22,
        fontWeight: '100',
        color: '#fff',
        textAlign: 'center',
        marginTop: 8
    },
    headerJob: {
        fontSize: 16,
        fontWeight: '100',
        color: '#fff',
        textAlign: 'center'
    },
    headerTile: {
        fontSize: 16,
        fontWeight: '100',
        color: 'rgba(0,0,0,.6)',
        marginTop: 15,
        textAlign: 'center'
    },
    columnas: {
        borderWidth: 1, 
        borderColor: 'rgba(0,0,0,.1)',
        backgroundColor: 'rgba(255,255,255,1)',  
        justifyContent: 'center', 
        alignItems: 'center',
        height: 120
    },
    profile: {
        justifyContent: 'center', 
        alignItems: 'center', 
        marginTop: 22
    },
    actionButtonIcon: {
        height: 22,
        color: 'white',
    },
    container: {
        flex: 1,
        padding: 10
    },
    backgroundImage: {
        width: null,
        height: 300,
    },
    overlay: {
        position: 'absolute',
        top: 0,
        right: 0,
        bottom: 0,
        left: 0,
        backgroundColor: 'red',
        opacity: 0.3
    },
    modal: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    modal4: {
        height: 300
    },
    text: {
        color: "black",
        fontSize: 22
    },
    btn: {
        margin: 10,
        backgroundColor: "#3B5998",
        color: "white",
        padding: 10
    },

    btnModal: {
        position: "absolute",
        top: 0,
        right: 0,
        width: 50,
        height: 50,
        backgroundColor: "transparent"
    },
    button: {
        backgroundColor: 'lightblue',
        padding: 12,
        margin: 16,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        borderColor: 'rgba(0, 0, 0, 0.1)',
    },
    modalHeaderProspecto: {
        backgroundColor: '#fcfcfc',
        padding: 10,
        borderColor: 'rgba(0, 0, 0, 0.1)',
        height: 350
    },
    modalHeaderVinculo: {
        backgroundColor: 'rgba(189,9,38,1)',
        padding: 10,
        borderColor: 'rgba(0, 0, 0, 0.1)',
        height: 350
    },
    modalAvatar: {
        alignItems: 'center',
        flex: 1
    },
    modalContent: {
        backgroundColor: 'rgba(255,255,255,1)',
        height: 70,
        borderColor: 'rgba(0, 0, 0, 0.1)',
        alignItems: 'center', 
        justifyContent: 'center'
    },
    bottomModal: {
        justifyContent: 'flex-end',
        margin: 0,
    }
});


AppRegistry.registerComponent("TabsP", ()=> TabsP);