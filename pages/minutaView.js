import React from 'react';

import {
    AppRegistry,
    ScrollView,
    View,
    TouchableOpacity,
    Platform,
    StyleSheet,
    LayoutAnimation, 
    AsyncStorage,
    Dimensions
} from 'react-native';
import RNFetchBlob from 'react-native-fetch-blob'
import Share, {ShareSheet, Button} from 'react-native-share';

import {
    Container,
    Content,
    List,
    ListItem,
    Left,
    Body,
    Right,
    Thumbnail,
    Text,
    Icon
} from 'native-base';
import {Avatar} from 'react-native-elements';
import ActionButton from 'react-native-action-button';
import Display from 'react-native-display';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { ImagePicker, Components, BlurView } from 'expo';
import Animation from 'lottie-react-native';
import TimerMixin from 'react-timer-mixin';
import {StackNavigator} from 'react-navigation';
let window = Dimensions.get('window')

export default class minutaPDF extends React.Component {
    constructor(props) {
        super(props);
         this.navigation = this.props.navigation;
        this.params = this.props.navigation.state;
        this.timeouts = [];
        this.state = {
            loaded: false,
            url: null
            
        }
    }
static navigationOptions = ({navigation}) => {
   

    return {
      title: 'Minutas',
      headerTintColor: '#FFF',
    
      headerStyle: {
        backgroundColor: '#41413f'
      }
    }
  };
    getPDF(){
          

    let url = 'http://www.innovatio.mx/reactAcem/pdfGenerator.php?id='+ this.navigation.state.params.id + "&idMinuta=" + this.navigation.state.params.idMinuta;  

        console.log(url);


    RNFetchBlob
  .config({
    fileCache : true,
    // by adding this option, the temp files will have a file extension
    appendExt : 'pdf'
  })
  .fetch('GET', url, {
    //some headers ..
    'Content-Type': 'application/PDF'
  })
  .then((res) => {

    

    this.setState({url: Platform.OS === 'android' ? 'file://' + res.path()  : '' + res.path(), loaded: true});

    // the temp file path with file extension `png`
    console.log('The file saved to ', this.state.url);
    // Beware that when using a file path as Image source on Android,
    // you must prepend "file://"" before the file path
  })
    }


componentDidMount() {

this.animation.play();
 this.timeouts.push(setTimeout(() => {
    this.setState({ timePassed: true });
  }, 3250));

this.getPDF();
}
    
styles = StyleSheet.create({
    pdf: {
        width: window.width,
          height: window.height,
    }
});




     render(){
         if(!this.state.loaded){
        return (
        <Display
          enter={'fadeIn'}
          exit={'fadeOut'}
          enable={!this.state.loaded}
          style={{
              
          position: 'absolute',
          zIndex: 100000,
          width: window.width,
          height: window.height,
          
        }}>
          <BlurView
            tint="dark"
            intensity={100}
            style={{
            position: 'absolute',
            zIndex: 100,
            width: window.width,
            height: window.height - 50
          }}>
            <View
              style={{
              flex: 1,
              justifyContent: 'center',
              alignContent: 'center'
            }}>
              <View
                style={{
                justifyContent: 'center',
                alignContent: 'center',
                width: window.width,
                marginLeft: window.width / 2 - 100
              }}>
                <Animation
                  style={{
                  width: 200,
                  height: 200
                }}
                  source={require('../assets/data12.json')}
                  loop
                  ref={(c) => this.animation = c}/>
              </View>
              <Text
                style={{
                textAlign: 'center',
                color: 'white',
                width: window.width
              }}>Cargando...</Text>

            </View>
          </BlurView>
        </Display>
      )
    }


    return(
        
      <PDFView ref={(pdf)=>{this.pdfView = pdf;}}
                         path={this.state.url}
                         onLoadComplete = {(pageCount)=>{
                            this.pdfView.setNativeProps({
                                zoom: 1.5
                            });
                         }}
                         style={this.styles.pdf}/>

    );                     
    }
    

}

AppRegistry.registerComponent("minutaView", ()=> minutaView);
