import React from 'react';
import {
  AppRegistry,
  ScrollView,
  View,
  TouchableOpacity,
  Platform,
  StyleSheet,
  LayoutAnimation,
  Animated,
  Dimensions,
  AsyncStorage,
  Alert, Navigator, Image
} from 'react-native';
import {
  Container,
  Content,
  List,
  ListItem,
  Left,
  Body,
  Right,
  Thumbnail,
  Text,
  Icon,
   Button
} from 'native-base';
import {Avatar} from 'react-native-elements';
import ActionButton from 'react-native-action-button';
import Animation from 'lottie-react-native';
import TimerMixin from 'react-timer-mixin';
import {ImagePicker, Components, BlurView} from 'expo';
import Communications from 'react-native-communications';
import Modal from 'react-native-modalbox';

import Display from 'react-native-display';
let window = Dimensions.get('window')

import {StackNavigator} from 'react-navigation';
console.ignoredYellowBox = ['Warning: checkPropTypes'];

export default class Prospectos extends React.Component {

  constructor(props) {
    super(props);
    this.navigation = this.props.navigation;
    this.params = this.props.navigation.state;
    this.timeouts = [];

    this.state = {
      progress: new Animated.Value(0),
      timePassed: false,
      isActionButtonVisible: true,
      prospectosArr: [],
      dataLoaded: false,
       isOpen: false,
      isDisabled: false,
      swipeToClose: true,
      sliderValue: 0.3,
      showImage: false, 
      activeImage: null,
      activeId: null
    };

  }

onClose() {
    console.log('Modal just closed');
  }

  onOpen() {
    console.log('Modal just openned');
  }

  onClosingState(state) {
    console.log('the open/close of the swipeToClose just changed');
  }


  static navigationOptions = ({navigation}) => {
    const {
      params = {}
    } = navigation.state
    const {
      nav = {}
    } = navigation;

    return {
      title: 'Prospectos',
      headerTintColor: '#FFF',
      headerRight: (Platform.OS === 'ios')
        ? <TouchableOpacity onPress={() => params.createProspecto(navigation, params)}><Icon
            name='ios-add'
            style={{
            fontSize: 27,
            color: 'white',
            marginRight: 20
          }}/></TouchableOpacity>
        : null,
      headerStyle: {
        backgroundColor: '#41413f'
      }
    }
  };

  _listViewOffset = 0

  _onScroll = (event) => {
    // Simple fade-in / fade-out animation
    const CustomLayoutLinear = {
      duration: 100,
      create: {
        type: LayoutAnimation.Types.linear,
        property: LayoutAnimation.Properties.opacity
      },
      update: {
        type: LayoutAnimation.Types.linear,
        property: LayoutAnimation.Properties.opacity
      },
      delete: {
        type: LayoutAnimation.Types.linear,
        property: LayoutAnimation.Properties.opacity
      }
    }
    // Check if the user is scrolling up or down by confronting the new scroll
    // position with your own one
    const currentOffset = event.nativeEvent.contentOffset.y
    const direction = (currentOffset > 0 && currentOffset > this._listViewOffset)
      ? 'down'
      : 'up'
    // If the user is scrolling down (and the action-button is still visible) hide
    // it
    const isActionButtonVisible = direction === 'up'
    if (isActionButtonVisible !== this.state.isActionButtonVisible) {
      LayoutAnimation.configureNext(CustomLayoutLinear)
      this.setState({isActionButtonVisible})
    }
    // Update your scroll position
    this._listViewOffset = currentOffset

  }

  deleteProspecto(id){
    fetch('http://innovatio.mx/reactAcem/deleteProspecto.php',{
      method: 'POST',
      header: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        id: this.navigation.state.params.id,
        idProspecto: id
      })
    }).then((response) =>  response.json()).then((responseJSON) => {
      if(responseJSON.result == "exito"){
      Alert.alert("Prospecto borrado exitosamente");
      this.getProspectos(this.navigation.state.params.id);
      this.forceUpdate();
    }else{
      Alert.alert("Error", "Este Prospecto esta asociado a uno o más vinculos. Borra los vinculos asociados e intenta de nuevo");
    }

      
    }).catch((error) => {
      console.error(error);
    }); 
  }

  getProspectos(id) {
    fetch('http://innovatio.mx/reactAcem/getProspectos.php', {
      method: 'POST',
      header: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({id: id})
    }).then((response) => response.json()).then((responseJSON) => {
      if(responseJSON.result != 'vacio'){
      this.setState({prospectosArr: responseJSON});
      this.setState({dataLoaded: true}
      )}else{
        console.log("No hay propectos registrados en esta cuenta");
        this.setState({dataLoaded: true});

      }
      
    }).catch((error) => {
      console.error(error);
    });
  }

  UploadProspectoImage(id, base64){
     fetch('http://innovatio.mx/reactAcem/addProspectoImage.php', {
      method: 'POST',
      header: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        id: this.navigation.state.params.id,
        idProspecto: id,
        baseImage: base64
        
      })
    }).then((response) => response.json()).then((responseJSON) => {
      this.getProspectos(this.navigation.state.params.id);
      this.forceUpdate();
    }).catch((error) => {
      console.error(error);
    });
  }

  async editProspecto( nombre, apellido_pat, apellido_mat, correo, telefono_oficina, celular, puesto, empresa, idProspecto){
    
    console.log("Pre-edit")
    const DEMO_TOKEN = await AsyncStorage.getItem('id_token2');
    this.navigation.navigate('createProspecto', {
      onGoBack: (text) => this.saveDetails(text),
      id: this.navigation.state.params.id,
      isEditing: true,
      nombre: nombre,
      apellido_pat: apellido_pat,
      apellido_mat: apellido_mat, 
      correo: correo,
      telefono_oficina: telefono_oficina,
      celular: celular,
      puesto: puesto,
      empresa: empresa,
      idProspecto: idProspecto,
      title: "Editar Prospecto"
    })
  }


  async getNewProspecto(navigation, params) {
    const DEMO_TOKEN = await AsyncStorage.getItem('id_token2');
    navigation.navigate('createProspecto', {
      onGoBack: (text) => params.handleSave(text),
      id: navigation.state.params.id, 
      title: "Crear Prospecto"
    });
  }

  saveDetails(text) {
    this.getProspectos(this.navigation.state.params.id);
    this.alertSuccess(text);
    this.forceUpdate();
  }

  alertSuccess(text) {
    Alert.alert("Prospecto "+ text + " exitosamente");
  }

  async getNewProspectoAndroid() {
    const DEMO_TOKEN = await AsyncStorage.getItem('id_token2');
    this
      .navigation
      .navigate('createProspecto', {
        onGoBack: (text) => this.saveDetails(text),
        id: this.navigation.state.params.id,
        title: "Crear Prospecto"
      });
  }

  getFab() {
    return (Platform.OS === 'android')
      ? <ActionButton
          onPress={() => this.getNewProspectoAndroid()}
          buttonTextStyle={{ color: '#41413f' }} buttonColor="#fff"></ActionButton>
      : null
  }

async getPhoto(id){
  result = await ImagePicker.launchCameraAsync(
    {
      quality: 0.5,
      aspect: [1, 1],
      allowsEditing: true,
      base64: true
    }
  );
  if(!result.cancelled){
    this.UploadProspectoImage(id, result.base64);
  }
}

  getAvatar( i ,url ,nombre, apellido, idP){

    if(url == "null"){
      return   <Avatar
                      key={i + "Avatar"}
                      small
                      rounded
                      title={nombre.charAt(0) + apellido.charAt(0)}
                      overlayContainerStyle={{
                      backgroundColor: '#cfcabb'
                    }}
                      onPress={() => this.getPhoto(idP)}
                      activeOpacity={0.7}/>;
            
    }else{
      return (  
        <TouchableOpacity onPress={()=>this.setState({showImage: true, activeImage: url, activeId: idP})}>
      <Thumbnail  small   source={{uri: url}} />
        </TouchableOpacity>
      );

    }
  }

renderPhoto(){
  return(
    <View>
    <Thumbnail
           style={{flex: 1}}
          resizeMode="contain" style={{width: 300,  height: 300 }}  source={{ uri: 'http://innovatio.mx/reactAcem/prospectosImages/5964f841cfb95.png' }}
        />
    </View>
  );
}

  componentDidMount() {
    console.log(this.navigation.state.params.id);
    this.getProspectos(this.navigation.state.params.id);
    this
.props
      .navigation
      .setParams({
        handleSave: this
          .saveDetails
          .bind(this),
        createProspecto: this
          .getNewProspecto
          .bind(this)
      });

    this
      .animation
      .play();
    this
      .timeouts
      .push(setTimeout(() => {
        this.setState({timePassed: true});

      }, 3000));
  }

  clearTimeouts() {
    this
      .timeouts
      .forEach(clearTimeout);
  }

  componentWillUnmount() {
    this.clearTimeouts();
  }

  render() {

        var Lightbox = require('react-native-lightbox');

    let that = this;
            Swipeout = require('react-native-swipeout').default;

    if (!this.state.timePassed || !this.state.dataLoaded) {
      return (
        <Display
          enter={'fadeIn'}
          exit={'fadeOut'}
          enable={!this.state.timePassed}
          style={{
          position: 'absolute',
          zIndex: 100000,
          width: window.width,
          height: window.height
        }}>
          <BlurView
            tint="dark"
            intensity={100}
            style={{
            position: 'absolute',
            zIndex: 100,
            width: window.width,
            height: window.height - 50
          }}>
            <View
              style={{
              flex: 1,
              justifyContent: 'center',
              alignContent: 'center'
            }}>
              <View
                style={{
                justifyContent: 'center',
                alignContent: 'center',
                width: window.width,
                marginLeft: window.width / 2 - 100
              }}>
                <Animation
                  style={{
                  width: 200,
                  height: 200
                }}
                  source={require('../assets/data12.json')}
                  loop
                  ref={(c) => this.animation = c}/>
              </View>
              <Text
                style={{
                textAlign: 'center',
                color: 'white',
                width: window.width
              }}>Cargando...</Text>

            </View>
          </BlurView>
        </Display>
      )
    }

    return (
      <Container>
        <Content onScroll={this._onScroll}>
          <List style={{
            backgroundColor: 'white'
          }}>


            {(this.state.prospectosArr).map((l, i) => (
              <Swipeout
                key={"swipe" + i}
                style={{
                backgroundColor: 'white'
              }}
                right={[
                {
                  text: <Icon
                    style={{
                    fontSize: 27,
                    color: 'white'
                  }}
                    ios='ios-cog'
                    android='md-cog'
                    color='white'></Icon>,
                    backgroundColor: 'rgb(255,204,0)',
                    onPress: ()=> {this.editProspecto( l.nombre, l.apellido_pat, l.apellido_mat, l.correo, l.telefono_oficina, l.celular, l.puesto, l.empresa, l.id)}
                },
                {
              
                  text: <Icon
                    style={{
                    fontSize: 27,
                    color: 'white'
                  }}
                    ios='ios-mail-outline'
                    android='md-mail'
                    color='white'></Icon>,
                  backgroundColor: 'rgb(76,217,100)',
                  onPress: ()=> Communications.email([l.correo], null, null, null, null)
                }, {
                  text: <Icon
                    style={{
                    fontSize: 27,
                    color: 'white'
                  }}
                    ios='ios-call-outline'
                    android='md-call'
                    color='white'></Icon>,
                  backgroundColor: 'rgb(0,122,255)',
                  onPress: ()=> 
                  Alert.alert(
                    "Llamar a:",
                    null, 
                    [
                      {text: "Celular", onPress: ()=>Communications.phonecall(l.celular, true)},
                      {text: "Oficina", onPress: ()=>Communications.phonecall(l.telefono_oficina, true)},
                      {text: "Cancelar"}
                    ]
                  )
                  

                }, {
                  text: <Icon
                    style={{
                    fontSize: 27,
                    color: 'white'
                  }}
                    ios='ios-trash-outline'
                    android='md-trash'
                    color='white'></Icon>,
                  backgroundColor: 'rgb(255,59,48)',
                  onPress: ()=> Alert.alert("¿Estas seguro de que quieres borrar este prospecto?", "Se borrarán todos los datos asociados a este prospecto",
                  [
                    {text: "Cancelar"},
                    {text: "Aceptar", onPress: ()=> this.deleteProspecto(l.id)}
                  ]
                  )
                }
              ]}>
                
       

                <ListItem key={i + "Item"}  onPress={()=>this.setState({showImage: true, activeImage: l.imageUrl, activeId: l.id})} avatar>

                  <Left key={i + "left"}>

                    {this.getAvatar(i, l.imageUrl, l.nombre, l.apellido_pat, l.id)}



                  </Left>
                  <Body key={i + "body"}>

                    <Text key={i + "Text1"}>{l.nombre} {l.apellido_mat} {l.apellido_pat}</Text>
                    <Text key={i + "Text2"} note>{l.empresa}
                      / {l.puesto}</Text>

                  </Body>

                </ListItem>
                </Swipeout>
            ))}

          </List>

        </Content>


        <Display
          enter={'fadeIn'}
          exit={'fadeOut'}
          enable={this.state.showImage}
          style={{
          position: 'absolute',
          zIndex: 100000,
          width: window.width,
          height: window.height
        }}>
          <BlurView
            tint="dark"
            intensity={100}
            style={{
            position: 'absolute',
            zIndex: 100,
            justifyContent: 'center',
            alignContent: 'center',
            width: window.width,
            height: window.height
          }}>
             <View style={{ flex: 1,         justifyContent: 'space-between', flexDirection: 'row'  }} onPress={()=> this.setState({showImage: false})}>
            <Icon
            ios='ios-close-circle-outline'
            android='md-close-circle'
            style={{
            fontSize: 35,
            color: 'white',
            marginTop: 40, 
            marginLeft: 20
          }}
          onPress={()=> this.setState({showImage: false})}/>             
          <Icon
            ios='ios-camera-outline'
            android='md-camera'
            style={{
            fontSize: 35,
            color: 'white',
            marginTop: 40, 
            marginRight: 20
          
          }}
          onPress={()=> {this.getPhoto(this.state.activeId);
            this.setState({showImage: false})
          }}/>             

                   
           </View>
           

         <View  style={{flex: 9, alignContent: 'center', justifyContent: 'center'}}>
              <Image           style={{ height: window.width-25, width: window.width -25, marginLeft: 12.5, marginRight: 12.5, marginBottom: 100  }}
 source={{uri: this.state.activeImage}}/>
               </View>


          </BlurView>
        </Display>
                        {this.getFab()}

      </Container>
    )
  }

}
async function getNewProspecto(navigation, state) {
  const DEMO_TOKEN = await AsyncStorage.getItem('id_token2');
  navigation.navigate('createProspecto', {
    onGoBack: (text) => state.params.handleSave(text),
    id: navigation.state.params.id,
    title: "Crear Prospecto"
  });
}


const styles = StyleSheet.create({
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: 'white'
  },

  wrapper: {
    paddingTop: 50,
    flex: 1
  },

  modal: {
    justifyContent: 'center',
    alignItems: 'center'
  }, 
  modal2: {
    height: 230,
    backgroundColor: "#3B5998"
  },

  modal3: {
    height: 300,
    width: 300
  },

  modal4: {
    height: 300
  },

  btn: {
    margin: 10,
    backgroundColor: "#3B5998",
    color: "white",
    padding: 10
  },

  btnModal: {
    position: "absolute",
    top: 0,
    right: 0,
    width: 50,
    height: 50,
    backgroundColor: "transparent"
  },

  text: {
    color: "black",
    fontSize: 22
  }

});
AppRegistry.registerComponent("Prospectos", () => Prospectos);
